package com.rollong.common.test.jwt

import com.rollong.common.jwt.JwtConfig
import com.rollong.common.jwt.JwtHelper
import org.junit.Test

class TokenTest {

    @Test
    fun assign() {
        val helper = JwtHelper(
                config = JwtConfig()
        )
        val token = helper.issue(
                userType = "test",
                userName = "user@test",
                scope = listOf("test", "test2")
        )
        val content = helper.parse(token = token.token)
        assert(true == content.scope?.contains("test2"))
    }
}