package com.rollong.common.test.inetAddress

import com.rollong.common.inetAdress.InetAddressUtil
import org.junit.Test

class InetAddressTest {

    @Test
    fun mask() {
        val mask = InetAddressUtil.mask(
                length = 10,
                size = 16
        )
        assert(mask[0].toInt() == -1)
    }

    @Test
    fun cidr() {
        val long1 = InetAddressUtil.ipv4toLong("255.255.255.255")
        val long2 = InetAddressUtil.ipv4toLong("192.168.3.1")
        val cidr = InetAddressUtil.CIDR.fromString("192.168.3.1/24")
        assert(cidr.inRange(ipString = "192.168.3.22"))
        assert(!cidr.inRange(ipString = "192.168.4.22"))
    }
}