package com.rollong.common.excel.data

@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class RowWidth(val width: Int = 200)
