package com.rollong.common.excel.data

import com.rollong.common.excel.SheetData
import com.rollong.common.util.format
import com.rollong.common.util.toDatetime
import java.math.BigDecimal
import java.util.*
import kotlin.math.roundToInt
import kotlin.math.roundToLong
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.javaField
import kotlin.reflect.jvm.javaMethod
import kotlin.reflect.jvm.javaType

object ExcelDataUtil {

    fun <T : Any> emptySheet(template: T): SheetData {
        val sheet = this.dataToSheet(listOf(template))
        return SheetData(
                index = sheet.index
        )
    }

    class P(
            val title: String,
            val value: Any?,
            val order: Int
    )

    fun <T : Any> dataToSheet(payload: List<T>): SheetData {
        val data = mutableListOf<Map<String, Any?>>()
        payload.forEach {
            val row = it
            val line = mutableListOf<P>()
            val map = mutableMapOf<String, Any?>()
            for (memberProperty in row.javaClass.kotlin.memberProperties) {
                val anno = memberProperty.findAnnotation<ExcelColumn>()
                        ?: memberProperty.getter.findAnnotation<ExcelColumn>()
                if (null != anno) {
                    val title = if (anno.title.isNotBlank()) {
                        anno.title
                    } else {
                        memberProperty.name
                    }
                    val order = anno.order

                    val dateFormat = memberProperty.findAnnotation<ExcelDateFormat>()
                            ?: memberProperty.getter.findAnnotation<ExcelDateFormat>()

                    var value = memberProperty.get(row)
                    if (null != dateFormat) {
                        val pattern = dateFormat.pattern
                        val zone = dateFormat.timezone
                        value = when (value) {
                            is Date -> value.format(pattern = pattern, zone = zone)
                            is Long -> Date(value).format(pattern = pattern, zone = zone)
                            is Int -> Date(value * 1000L).format(pattern = pattern, zone = zone)
                            null -> null
                            else -> throw RuntimeException("无法将${it.javaClass.name}类型的对象转换为Date类型")
                        }
                    }
                    line.add(P(
                            title = title,
                            order = order,
                            value = value
                    ))
                }
            }
            for (p in line.sortedBy { it.order }) {
                map[p.title] = p.value
            }
            data.add(map)
        }
        return SheetData(data)
    }

    fun <T : Any> sheetToData(
            sheetData: SheetData,
            factory: () -> T
    ): List<T> {
        val result = mutableListOf<T>()
        for (map in sheetData.iterator()) {
            val row = factory.invoke()
            for (memberProperty in row.javaClass.kotlin.memberProperties) {
                if (memberProperty is KMutableProperty<*>) {
                    val anno = memberProperty.findAnnotation<ExcelColumn>()
                            ?: memberProperty.setter.findAnnotation<ExcelColumn>()
                    if (null != anno) {
                        val title = if (anno.title.isNotBlank()) {
                            anno.title
                        } else {
                            memberProperty.name
                        }
                        val valueFromExcel = map.getOrDefault(title, null)
                        val field = memberProperty.javaField
                        val type = if (null != field) {
                            field.type
                        } else {
                            Class.forName(memberProperty.returnType.javaType.typeName)
                        }
                        if (null == valueFromExcel) {

                        } else if (type == valueFromExcel.javaClass) {
                            memberProperty.setter.javaMethod?.invoke(row, valueFromExcel)
                        } else {
                            val dateFormat = memberProperty.findAnnotation<ExcelDateFormat>()
                                    ?: memberProperty.getter.findAnnotation<ExcelDateFormat>()
                            when (type) {
                                String::class.java, java.lang.String::class.java -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toString())
                                Int::class.java, java.lang.Integer::class.java -> when (valueFromExcel) {
                                    is String -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.trim().toDoubleOrNull()?.roundToInt())
                                    is Double -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.roundToInt())
                                    is Boolean -> memberProperty.setter.javaMethod?.invoke(row, if (valueFromExcel) 1 else 0)
                                    is Int -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel)
                                }
                                Long::class.java, java.lang.Long::class.java -> {
                                    when (valueFromExcel) {
                                        is String -> {
                                            if (null != dateFormat) {
                                                val pattern = dateFormat.pattern
                                                val zone = dateFormat.timezone
                                                memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.trim().toDatetime(pattern = pattern, zone = zone).toDate().time)
                                            } else {
                                                memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.trim().toDoubleOrNull()?.roundToLong())
                                            }
                                        }
                                        is Double -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.roundToLong())
                                        is Boolean -> memberProperty.setter.javaMethod?.invoke(row, if (valueFromExcel) 1 else 0)
                                        is Long -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel)
                                    }
                                }
                                Double::class.java -> {
                                    when (valueFromExcel) {
                                        is String -> {
                                            memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.trim().toDoubleOrNull())
                                        }
                                        is Boolean -> memberProperty.setter.javaMethod?.invoke(row, if (valueFromExcel) 1.toDouble() else 0.toDouble())
                                        is Long -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toDouble())
                                        is BigDecimal -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toDouble())
                                        is Float -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toDouble())
                                        is Double -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel)
                                    }
                                }
                                Float::class.java -> {
                                    when (valueFromExcel) {
                                        is String -> {
                                            memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.trim().toFloatOrNull())
                                        }
                                        is Boolean -> memberProperty.setter.javaMethod?.invoke(row, if (valueFromExcel) 1.toFloat() else 0.toFloat())
                                        is Long -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toFloat())
                                        is BigDecimal -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toFloat())
                                        is Float -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel)
                                        is Double -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toFloat())
                                    }
                                }
                                BigDecimal::class.java -> {
                                    when (valueFromExcel) {
                                        is String -> {
                                            memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.trim().toBigDecimalOrNull())
                                        }
                                        is Boolean -> memberProperty.setter.javaMethod?.invoke(row, if (valueFromExcel) BigDecimal.ONE else BigDecimal.ZERO)
                                        is Long -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toBigDecimal())
                                        is BigDecimal -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel)
                                        is Float -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toBigDecimal())
                                        is Double -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.toBigDecimal())
                                    }
                                }
                                Date::class.java -> when (valueFromExcel) {
                                    is String -> {
                                        val pattern = dateFormat?.pattern ?: "yyyy-MM-dd HH:mm:ss"
                                        memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.trim().toDatetime(pattern = pattern).toDate())
                                    }
                                    is Double -> memberProperty.setter.javaMethod?.invoke(row, Date(valueFromExcel.roundToLong()))
                                    is Boolean -> memberProperty.setter.javaMethod?.invoke(row, null)
                                    is Long -> memberProperty.setter.javaMethod?.invoke(row, Date(valueFromExcel))
                                }
                                Boolean::class.java, java.lang.Boolean::class.java -> when (valueFromExcel) {
                                    is String -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel.trim().toBoolean())
                                    is Double -> memberProperty.setter.javaMethod?.invoke(row, 0.0 != valueFromExcel)
                                    is Boolean -> memberProperty.setter.javaMethod?.invoke(row, valueFromExcel)
                                }
                                else -> throw RuntimeException("无法解析的excel目标类型 ${type.name}")
                            }
                        }
                    }
                }
            }
            result.add(row)
        }
        return result
    }
}