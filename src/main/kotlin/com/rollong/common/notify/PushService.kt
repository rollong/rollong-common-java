package com.rollong.common.notify

import com.rollong.common.exception.APIRuntimeException
import com.rollong.common.notify.grpc.payload.Message
import com.rollong.common.notify.grpc.push.BroadcastRequest
import com.rollong.common.notify.grpc.push.PushRequest
import com.rollong.common.notify.grpc.push.PushServiceGrpc
import java.util.*

abstract class PushService : BaseNotifyService() {

    fun push(
            title: String,
            content: String,
            alias: String,
            extras: Map<String, String>? = null,
            scheduledAt: Date? = null,
            config: Config
    ) {
        val channel = this.channel(config)
        try {
            val message = Message.newBuilder()
            message.title = title
            message.content = content
            extras?.let {
                message.putAllExtras(it.toMutableMap())
            }

            val request = PushRequest.newBuilder()
            request.clientId = config.clientId
            request.clientSecret = config.clientSecret
            request.alias = alias
            request.scheduledAt = scheduledAt?.time ?: 0
            request.message = message.build()

            val stub = PushServiceGrpc.newBlockingStub(channel).withCompression("gzip")
            val response = stub.push(request.build())
            if (0 != response.code) {
                throw APIRuntimeException(
                        errCode = response.code,
                        message = response.message
                )
            }
        } catch (e: Exception) {
            throw e
        } finally {
            channel.shutdownNow()
        }
    }

    fun broadcast(
            title: String,
            content: String,
            tags: Set<String>? = null,
            tagsAnd: Boolean = false,
            platforms: Set<BroadcastRequest.Platform>? = null,
            extras: Map<String, String>? = null,
            scheduledAt: Date? = null,
            config: Config
    ) {
        val channel = this.channel(config)
        try {
            val message = Message.newBuilder()
            message.title = title
            message.content = content
            extras?.let {
                message.putAllExtras(it.toMutableMap())
            }

            val request = BroadcastRequest.newBuilder()
            request.clientId = config.clientId
            request.clientSecret = config.clientSecret
            if (null != tags) {
                request.addAllTags(tags.toMutableList())
            } else {
                request.all = true
            }
            request.tagsAnd = tagsAnd
            if (null != platforms) {
                request.addAllPlatforms(platforms.toMutableList())
            } else {
                request.addPlatforms(BroadcastRequest.Platform.ALL)
            }
            request.scheduledAt = scheduledAt?.time ?: 0
            request.message = message.build()

            val stub = PushServiceGrpc.newBlockingStub(channel).withCompression("gzip")
            val response = stub.broadcast(request.build())
            if (0 != response.code) {
                throw APIRuntimeException(
                        errCode = response.code,
                        message = response.message
                )
            }
        } catch (e: Exception) {
            throw e
        } finally {
            channel.shutdownNow()
        }
    }
}