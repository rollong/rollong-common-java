package com.rollong.common.notify

import com.rollong.common.gRPC.ChannelBuilder

abstract class BaseNotifyService {

    protected fun channel(config: Config) = ChannelBuilder()
            .address(config.address)
            .port(config.port)
            .ssl(config.ssl)
            .build()
}