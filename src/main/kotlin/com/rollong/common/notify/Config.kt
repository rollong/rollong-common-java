package com.rollong.common.notify

open class Config(
        var address: String = "notify-dev.ctgs.zhonghuixz.com",
        var port: Int = 6565,
        var ssl: Boolean = true,
        var clientId: String = "",
        var clientSecret: String = ""
)