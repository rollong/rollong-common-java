package com.rollong.common.notify

import com.rollong.common.exception.APIRuntimeException
import com.rollong.common.notify.grpc.payload.Message
import com.rollong.common.notify.grpc.sms.*
import java.util.concurrent.TimeUnit

abstract class SmsService : BaseNotifyService() {

    fun sendTemplate(
            mobile: String,
            templateCode: String,
            params: Map<String, String>,
            signName: String? = null,
            config: Config
    ) {
        val channel = this.channel(config)
        try {
            val request = SendTemplateRequest.newBuilder()
            val stub = SmsServiceGrpc.newBlockingStub(channel).withCompression("gzip")
            request.clientId = config.clientId
            request.clientSecret = config.clientSecret
            request.mobile = mobile
            val messageBuilder = Message.newBuilder()
            messageBuilder.templateCode = templateCode
            messageBuilder.putAllExtras(params)
            request.message = messageBuilder.build()
            signName?.let { request.signName = signName }
            val response = stub.sendTemplate(request.build())
            if (0 != response.code) {
                throw APIRuntimeException(
                        errCode = response.code,
                        message = response.message
                )
            }
        } catch (e: Exception) {
            throw e
        } finally {
            channel.shutdownNow()
        }
    }

    fun generateVerify(
            mobile: String,
            action: String,
            product: String,
            timeout: Long = 5,
            unit: TimeUnit = TimeUnit.MINUTES,
            config: Config
    ): GenerateVerifyCodeResponse {
        val channel = this.channel(config)
        try {
            val request = SendVerifySmsRequest.newBuilder()
            val stub = SmsServiceGrpc.newBlockingStub(channel).withCompression("gzip")
            request.clientId = config.clientId
            request.clientSecret = config.clientSecret
            request.mobile = mobile
            request.action = action
            request.product = product
            request.expireInSeconds = (unit.toSeconds(timeout)).toInt()
            val response = stub.generateVerifyCode(request.build())
            if (0 != response.code) {
                throw APIRuntimeException(
                        errCode = response.code,
                        message = response.message
                )
            }
            return response
        } catch (e: Exception) {
            throw e
        } finally {
            channel.shutdownNow()
        }
    }

    fun sendSMSVerify(
            mobile: String,
            action: String,
            product: String,
            signName: String? = null,
            timeout: Long = 5,
            unit: TimeUnit = TimeUnit.MINUTES,
            config: Config
    ) {
        val channel = this.channel(config)
        try {
            val request = SendVerifySmsRequest.newBuilder()
            val stub = SmsServiceGrpc.newBlockingStub(channel).withCompression("gzip")
            request.clientId = config.clientId
            request.clientSecret = config.clientSecret
            request.mobile = mobile
            request.action = action
            request.product = product
            request.expireInSeconds = (unit.toSeconds(timeout)).toInt()
            signName?.let { request.signName = signName }
            val response = stub.sendVerifyCode(request.build())
            if (0 != response.code) {
                throw APIRuntimeException(
                        errCode = response.code,
                        message = response.message
                )
            }
        } catch (e: Exception) {
            throw e
        } finally {
            channel.shutdownNow()
        }
    }

    fun checkSMSVerify(
            mobile: String,
            action: String,
            product: String,
            code: String,
            config: Config
    ): Boolean {
        val channel = this.channel(config)
        try {
            val request = CheckVerifySmsRequest.newBuilder()
            val stub = SmsServiceGrpc.newBlockingStub(channel).withCompression("gzip")
            request.clientId = config.clientId
            request.clientSecret = config.clientSecret
            request.mobile = mobile
            request.action = action
            request.product = product
            request.verifyCode = code
            val response = stub.checkVerifyCode(request.build())
            if (0 != response.code) {
                throw APIRuntimeException(
                        errCode = response.code,
                        message = response.message
                )
            }
            return response.passed
        } catch (e: Exception) {
            throw e
        } finally {
            channel.shutdownNow()
        }
    }
}