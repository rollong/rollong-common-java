// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: notify/sms.service.proto

package com.rollong.common.notify.grpc.sms;

public final class SmsServiceOuterClass {
    static final com.google.protobuf.Descriptors.Descriptor
            internal_static_notify_SendVerifySmsRequest_descriptor;
    static final
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
            internal_static_notify_SendVerifySmsRequest_fieldAccessorTable;
    static final com.google.protobuf.Descriptors.Descriptor
            internal_static_notify_CheckVerifySmsRequest_descriptor;
    static final
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
            internal_static_notify_CheckVerifySmsRequest_fieldAccessorTable;
    static final com.google.protobuf.Descriptors.Descriptor
            internal_static_notify_SendSmsResponse_descriptor;
    static final
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
            internal_static_notify_SendSmsResponse_fieldAccessorTable;
    static final com.google.protobuf.Descriptors.Descriptor
            internal_static_notify_GenerateVerifyCodeResponse_descriptor;
    static final
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
            internal_static_notify_GenerateVerifyCodeResponse_fieldAccessorTable;
    static final com.google.protobuf.Descriptors.Descriptor
            internal_static_notify_CheckVerifySmsResponse_descriptor;
    static final
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
            internal_static_notify_CheckVerifySmsResponse_fieldAccessorTable;
    static final com.google.protobuf.Descriptors.Descriptor
            internal_static_notify_SendTemplateRequest_descriptor;
    static final
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
            internal_static_notify_SendTemplateRequest_fieldAccessorTable;
    static final com.google.protobuf.Descriptors.Descriptor
            internal_static_notify_SendTemplateResponse_descriptor;
    static final
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
            internal_static_notify_SendTemplateResponse_fieldAccessorTable;
    private static com.google.protobuf.Descriptors.FileDescriptor
            descriptor;

    static {
        java.lang.String[] descriptorData = {
                "\n\030notify/sms.service.proto\022\006notify\032\034noti" +
                        "fy/payload.message.proto\"\237\001\n\024SendVerifyS" +
                        "msRequest\022\016\n\006mobile\030\001 \001(\t\022\016\n\006action\030\002 \001(" +
                        "\t\022\031\n\021expire_in_seconds\030\003 \001(\005\022\017\n\007product\030" +
                        "\004 \001(\t\022\021\n\tclient_id\030\t \001(\t\022\025\n\rclient_secre" +
                        "t\030\n \001(\t\022\021\n\tsign_name\030\013 \001(\t\"\207\001\n\025CheckVeri" +
                        "fySmsRequest\022\016\n\006mobile\030\001 \001(\t\022\016\n\006action\030\002" +
                        " \001(\t\022\023\n\013verify_code\030\003 \001(\t\022\017\n\007product\030\004 \001" +
                        "(\t\022\021\n\tclient_id\030\t \001(\t\022\025\n\rclient_secret\030\n" +
                        " \001(\t\"0\n\017SendSmsResponse\022\014\n\004code\030\001 \001(\005\022\017\n" +
                        "\007message\030\002 \001(\t\"b\n\032GenerateVerifyCodeResp" +
                        "onse\022\014\n\004code\030\001 \001(\005\022\017\n\007message\030\002 \001(\t\022\022\n\nv" +
                        "erifyCode\030\003 \001(\t\022\021\n\texpiresAt\030\004 \001(\003\"x\n\026Ch" +
                        "eckVerifySmsResponse\022\014\n\004code\030\001 \001(\005\022\017\n\007me" +
                        "ssage\030\002 \001(\t\022\016\n\006mobile\030\003 \001(\t\022\016\n\006action\030\004 " +
                        "\001(\t\022\017\n\007product\030\005 \001(\t\022\016\n\006passed\030\006 \001(\010\"\204\001\n" +
                        "\023SendTemplateRequest\022\016\n\006mobile\030\001 \001(\t\022 \n\007" +
                        "message\030\002 \001(\0132\017.notify.Message\022\021\n\tclient" +
                        "_id\030\003 \001(\t\022\025\n\rclient_secret\030\004 \001(\t\022\021\n\tsign" +
                        "_name\030\005 \001(\t\"5\n\024SendTemplateResponse\022\014\n\004c" +
                        "ode\030\001 \001(\005\022\017\n\007message\030\002 \001(\t2\312\002\n\nSmsServic" +
                        "e\022V\n\022GenerateVerifyCode\022\034.notify.SendVer" +
                        "ifySmsRequest\032\".notify.GenerateVerifyCod" +
                        "eResponse\022G\n\016SendVerifyCode\022\034.notify.Sen" +
                        "dVerifySmsRequest\032\027.notify.SendSmsRespon" +
                        "se\022P\n\017CheckVerifyCode\022\035.notify.CheckVeri" +
                        "fySmsRequest\032\036.notify.CheckVerifySmsResp" +
                        "onse\022I\n\014SendTemplate\022\033.notify.SendTempla" +
                        "teRequest\032\034.notify.SendTemplateResponseB" +
                        ",\n\"com.rollong.common.notify.grpc.smsP\001\242" +
                        "\002\003HLWb\006proto3"
        };
        com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
                new com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner() {
                    public com.google.protobuf.ExtensionRegistry assignDescriptors(
                            com.google.protobuf.Descriptors.FileDescriptor root) {
                        descriptor = root;
                        return null;
                    }
                };
        com.google.protobuf.Descriptors.FileDescriptor
                .internalBuildGeneratedFileFrom(descriptorData,
                        new com.google.protobuf.Descriptors.FileDescriptor[]{
                                com.rollong.common.notify.grpc.payload.PayloadMessage.getDescriptor(),
                        }, assigner);
        internal_static_notify_SendVerifySmsRequest_descriptor =
                getDescriptor().getMessageTypes().get(0);
        internal_static_notify_SendVerifySmsRequest_fieldAccessorTable = new
                com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
                internal_static_notify_SendVerifySmsRequest_descriptor,
                new java.lang.String[]{"Mobile", "Action", "ExpireInSeconds", "Product", "ClientId", "ClientSecret", "SignName",});
        internal_static_notify_CheckVerifySmsRequest_descriptor =
                getDescriptor().getMessageTypes().get(1);
        internal_static_notify_CheckVerifySmsRequest_fieldAccessorTable = new
                com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
                internal_static_notify_CheckVerifySmsRequest_descriptor,
                new java.lang.String[]{"Mobile", "Action", "VerifyCode", "Product", "ClientId", "ClientSecret",});
        internal_static_notify_SendSmsResponse_descriptor =
                getDescriptor().getMessageTypes().get(2);
        internal_static_notify_SendSmsResponse_fieldAccessorTable = new
                com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
                internal_static_notify_SendSmsResponse_descriptor,
                new java.lang.String[]{"Code", "Message",});
        internal_static_notify_GenerateVerifyCodeResponse_descriptor =
                getDescriptor().getMessageTypes().get(3);
        internal_static_notify_GenerateVerifyCodeResponse_fieldAccessorTable = new
                com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
                internal_static_notify_GenerateVerifyCodeResponse_descriptor,
                new java.lang.String[]{"Code", "Message", "VerifyCode", "ExpiresAt",});
        internal_static_notify_CheckVerifySmsResponse_descriptor =
                getDescriptor().getMessageTypes().get(4);
        internal_static_notify_CheckVerifySmsResponse_fieldAccessorTable = new
                com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
                internal_static_notify_CheckVerifySmsResponse_descriptor,
                new java.lang.String[]{"Code", "Message", "Mobile", "Action", "Product", "Passed",});
        internal_static_notify_SendTemplateRequest_descriptor =
                getDescriptor().getMessageTypes().get(5);
        internal_static_notify_SendTemplateRequest_fieldAccessorTable = new
                com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
                internal_static_notify_SendTemplateRequest_descriptor,
                new java.lang.String[]{"Mobile", "Message", "ClientId", "ClientSecret", "SignName",});
        internal_static_notify_SendTemplateResponse_descriptor =
                getDescriptor().getMessageTypes().get(6);
        internal_static_notify_SendTemplateResponse_fieldAccessorTable = new
                com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
                internal_static_notify_SendTemplateResponse_descriptor,
                new java.lang.String[]{"Code", "Message",});
        com.rollong.common.notify.grpc.payload.PayloadMessage.getDescriptor();
    }
    private SmsServiceOuterClass() {
    }

    public static void registerAllExtensions(
            com.google.protobuf.ExtensionRegistryLite registry) {
    }

    public static void registerAllExtensions(
            com.google.protobuf.ExtensionRegistry registry) {
        registerAllExtensions(
                (com.google.protobuf.ExtensionRegistryLite) registry);
    }

    public static com.google.protobuf.Descriptors.FileDescriptor
    getDescriptor() {
        return descriptor;
    }

    // @@protoc_insertion_point(outer_class_scope)
}
