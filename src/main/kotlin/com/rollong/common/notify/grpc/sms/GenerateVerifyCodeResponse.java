// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: notify/sms.service.proto

package com.rollong.common.notify.grpc.sms;

/**
 * Protobuf type {@code notify.GenerateVerifyCodeResponse}
 */
public final class GenerateVerifyCodeResponse extends
        com.google.protobuf.GeneratedMessageV3 implements
        // @@protoc_insertion_point(message_implements:notify.GenerateVerifyCodeResponse)
        GenerateVerifyCodeResponseOrBuilder {
    public static final int CODE_FIELD_NUMBER = 1;
    public static final int MESSAGE_FIELD_NUMBER = 2;
    public static final int VERIFYCODE_FIELD_NUMBER = 3;
    public static final int EXPIRESAT_FIELD_NUMBER = 4;
    private static final long serialVersionUID = 0L;
    // @@protoc_insertion_point(class_scope:notify.GenerateVerifyCodeResponse)
    private static final com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse DEFAULT_INSTANCE;
    private static final com.google.protobuf.Parser<GenerateVerifyCodeResponse>
            PARSER = new com.google.protobuf.AbstractParser<GenerateVerifyCodeResponse>() {
        @java.lang.Override
        public GenerateVerifyCodeResponse parsePartialFrom(
                com.google.protobuf.CodedInputStream input,
                com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                throws com.google.protobuf.InvalidProtocolBufferException {
            return new GenerateVerifyCodeResponse(input, extensionRegistry);
        }
    };

    static {
        DEFAULT_INSTANCE = new com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse();
    }

    private int code_;
    private volatile java.lang.Object message_;
    private volatile java.lang.Object verifyCode_;
    private long expiresAt_;
    private byte memoizedIsInitialized = -1;

    // Use GenerateVerifyCodeResponse.newBuilder() to construct.
    private GenerateVerifyCodeResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
    }

    private GenerateVerifyCodeResponse() {
        code_ = 0;
        message_ = "";
        verifyCode_ = "";
        expiresAt_ = 0L;
    }
    private GenerateVerifyCodeResponse(
            com.google.protobuf.CodedInputStream input,
            com.google.protobuf.ExtensionRegistryLite extensionRegistry)
            throws com.google.protobuf.InvalidProtocolBufferException {
        this();
        if (extensionRegistry == null) {
            throw new java.lang.NullPointerException();
        }
        int mutable_bitField0_ = 0;
        com.google.protobuf.UnknownFieldSet.Builder unknownFields =
                com.google.protobuf.UnknownFieldSet.newBuilder();
        try {
            boolean done = false;
            while (!done) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        done = true;
                        break;
                    case 8: {

                        code_ = input.readInt32();
                        break;
                    }
                    case 18: {
                        java.lang.String s = input.readStringRequireUtf8();

                        message_ = s;
                        break;
                    }
                    case 26: {
                        java.lang.String s = input.readStringRequireUtf8();

                        verifyCode_ = s;
                        break;
                    }
                    case 32: {

                        expiresAt_ = input.readInt64();
                        break;
                    }
                    default: {
                        if (!parseUnknownFieldProto3(
                                input, unknownFields, extensionRegistry, tag)) {
                            done = true;
                        }
                        break;
                    }
                }
            }
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
            throw e.setUnfinishedMessage(this);
        } catch (java.io.IOException e) {
            throw new com.google.protobuf.InvalidProtocolBufferException(
                    e).setUnfinishedMessage(this);
        } finally {
            this.unknownFields = unknownFields.build();
            makeExtensionsImmutable();
        }
    }

    public static final com.google.protobuf.Descriptors.Descriptor
    getDescriptor() {
        return com.rollong.common.notify.grpc.sms.SmsServiceOuterClass.internal_static_notify_GenerateVerifyCodeResponse_descriptor;
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(
            java.nio.ByteBuffer data)
            throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(
            java.nio.ByteBuffer data,
            com.google.protobuf.ExtensionRegistryLite extensionRegistry)
            throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data, extensionRegistry);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(
            com.google.protobuf.ByteString data)
            throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(
            com.google.protobuf.ByteString data,
            com.google.protobuf.ExtensionRegistryLite extensionRegistry)
            throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data, extensionRegistry);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(byte[] data)
            throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(
            byte[] data,
            com.google.protobuf.ExtensionRegistryLite extensionRegistry)
            throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data, extensionRegistry);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(java.io.InputStream input)
            throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3
                .parseWithIOException(PARSER, input);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(
            java.io.InputStream input,
            com.google.protobuf.ExtensionRegistryLite extensionRegistry)
            throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3
                .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseDelimitedFrom(java.io.InputStream input)
            throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3
                .parseDelimitedWithIOException(PARSER, input);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseDelimitedFrom(
            java.io.InputStream input,
            com.google.protobuf.ExtensionRegistryLite extensionRegistry)
            throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3
                .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(
            com.google.protobuf.CodedInputStream input)
            throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3
                .parseWithIOException(PARSER, input);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parseFrom(
            com.google.protobuf.CodedInputStream input,
            com.google.protobuf.ExtensionRegistryLite extensionRegistry)
            throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3
                .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse prototype) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }

    public static com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static com.google.protobuf.Parser<GenerateVerifyCodeResponse> parser() {
        return PARSER;
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
        return this.unknownFields;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
    internalGetFieldAccessorTable() {
        return com.rollong.common.notify.grpc.sms.SmsServiceOuterClass.internal_static_notify_GenerateVerifyCodeResponse_fieldAccessorTable
                .ensureFieldAccessorsInitialized(
                        com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.class, com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.Builder.class);
    }

    /**
     * <code>int32 code = 1;</code>
     */
    public int getCode() {
        return code_;
    }

    /**
     * <code>string message = 2;</code>
     */
    public java.lang.String getMessage() {
        java.lang.Object ref = message_;
        if (ref instanceof java.lang.String) {
            return (java.lang.String) ref;
        } else {
            com.google.protobuf.ByteString bs =
                    (com.google.protobuf.ByteString) ref;
            java.lang.String s = bs.toStringUtf8();
            message_ = s;
            return s;
        }
    }

    /**
     * <code>string message = 2;</code>
     */
    public com.google.protobuf.ByteString
    getMessageBytes() {
        java.lang.Object ref = message_;
        if (ref instanceof java.lang.String) {
            com.google.protobuf.ByteString b =
                    com.google.protobuf.ByteString.copyFromUtf8(
                            (java.lang.String) ref);
            message_ = b;
            return b;
        } else {
            return (com.google.protobuf.ByteString) ref;
        }
    }

    /**
     * <code>string verifyCode = 3;</code>
     */
    public java.lang.String getVerifyCode() {
        java.lang.Object ref = verifyCode_;
        if (ref instanceof java.lang.String) {
            return (java.lang.String) ref;
        } else {
            com.google.protobuf.ByteString bs =
                    (com.google.protobuf.ByteString) ref;
            java.lang.String s = bs.toStringUtf8();
            verifyCode_ = s;
            return s;
        }
    }

    /**
     * <code>string verifyCode = 3;</code>
     */
    public com.google.protobuf.ByteString
    getVerifyCodeBytes() {
        java.lang.Object ref = verifyCode_;
        if (ref instanceof java.lang.String) {
            com.google.protobuf.ByteString b =
                    com.google.protobuf.ByteString.copyFromUtf8(
                            (java.lang.String) ref);
            verifyCode_ = b;
            return b;
        } else {
            return (com.google.protobuf.ByteString) ref;
        }
    }

    /**
     * <code>int64 expiresAt = 4;</code>
     */
    public long getExpiresAt() {
        return expiresAt_;
    }

    @java.lang.Override
    public final boolean isInitialized() {
        byte isInitialized = memoizedIsInitialized;
        if (isInitialized == 1) return true;
        if (isInitialized == 0) return false;

        memoizedIsInitialized = 1;
        return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
            throws java.io.IOException {
        if (code_ != 0) {
            output.writeInt32(1, code_);
        }
        if (!getMessageBytes().isEmpty()) {
            com.google.protobuf.GeneratedMessageV3.writeString(output, 2, message_);
        }
        if (!getVerifyCodeBytes().isEmpty()) {
            com.google.protobuf.GeneratedMessageV3.writeString(output, 3, verifyCode_);
        }
        if (expiresAt_ != 0L) {
            output.writeInt64(4, expiresAt_);
        }
        unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
        int size = memoizedSize;
        if (size != -1) return size;

        size = 0;
        if (code_ != 0) {
            size += com.google.protobuf.CodedOutputStream
                    .computeInt32Size(1, code_);
        }
        if (!getMessageBytes().isEmpty()) {
            size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, message_);
        }
        if (!getVerifyCodeBytes().isEmpty()) {
            size += com.google.protobuf.GeneratedMessageV3.computeStringSize(3, verifyCode_);
        }
        if (expiresAt_ != 0L) {
            size += com.google.protobuf.CodedOutputStream
                    .computeInt64Size(4, expiresAt_);
        }
        size += unknownFields.getSerializedSize();
        memoizedSize = size;
        return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse)) {
            return super.equals(obj);
        }
        com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse other = (com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse) obj;

        boolean result = true;
        result = result && (getCode()
                == other.getCode());
        result = result && getMessage()
                .equals(other.getMessage());
        result = result && getVerifyCode()
                .equals(other.getVerifyCode());
        result = result && (getExpiresAt()
                == other.getExpiresAt());
        result = result && unknownFields.equals(other.unknownFields);
        return result;
    }

    @java.lang.Override
    public int hashCode() {
        if (memoizedHashCode != 0) {
            return memoizedHashCode;
        }
        int hash = 41;
        hash = (19 * hash) + getDescriptor().hashCode();
        hash = (37 * hash) + CODE_FIELD_NUMBER;
        hash = (53 * hash) + getCode();
        hash = (37 * hash) + MESSAGE_FIELD_NUMBER;
        hash = (53 * hash) + getMessage().hashCode();
        hash = (37 * hash) + VERIFYCODE_FIELD_NUMBER;
        hash = (53 * hash) + getVerifyCode().hashCode();
        hash = (37 * hash) + EXPIRESAT_FIELD_NUMBER;
        hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
                getExpiresAt());
        hash = (29 * hash) + unknownFields.hashCode();
        memoizedHashCode = hash;
        return hash;
    }

    @java.lang.Override
    public Builder newBuilderForType() {
        return newBuilder();
    }

    @java.lang.Override
    public Builder toBuilder() {
        return this == DEFAULT_INSTANCE
                ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
            com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        Builder builder = new Builder(parent);
        return builder;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<GenerateVerifyCodeResponse> getParserForType() {
        return PARSER;
    }

    @java.lang.Override
    public com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }

    /**
     * Protobuf type {@code notify.GenerateVerifyCodeResponse}
     */
    public static final class Builder extends
            com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
            // @@protoc_insertion_point(builder_implements:notify.GenerateVerifyCodeResponse)
            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponseOrBuilder {
        private int code_;
        private java.lang.Object message_ = "";
        private java.lang.Object verifyCode_ = "";
        private long expiresAt_;

        // Construct using com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.newBuilder()
        private Builder() {
            maybeForceBuilderInitialization();
        }

        private Builder(
                com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
            super(parent);
            maybeForceBuilderInitialization();
        }

        public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
            return com.rollong.common.notify.grpc.sms.SmsServiceOuterClass.internal_static_notify_GenerateVerifyCodeResponse_descriptor;
        }

        @java.lang.Override
        protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
            return com.rollong.common.notify.grpc.sms.SmsServiceOuterClass.internal_static_notify_GenerateVerifyCodeResponse_fieldAccessorTable
                    .ensureFieldAccessorsInitialized(
                            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.class, com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.Builder.class);
        }

        private void maybeForceBuilderInitialization() {
            if (com.google.protobuf.GeneratedMessageV3
                    .alwaysUseFieldBuilders) {
            }
        }

        @java.lang.Override
        public Builder clear() {
            super.clear();
            code_ = 0;

            message_ = "";

            verifyCode_ = "";

            expiresAt_ = 0L;

            return this;
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
            return com.rollong.common.notify.grpc.sms.SmsServiceOuterClass.internal_static_notify_GenerateVerifyCodeResponse_descriptor;
        }

        @java.lang.Override
        public com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse getDefaultInstanceForType() {
            return com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.getDefaultInstance();
        }

        @java.lang.Override
        public com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse build() {
            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse result = buildPartial();
            if (!result.isInitialized()) {
                throw newUninitializedMessageException(result);
            }
            return result;
        }

        @java.lang.Override
        public com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse buildPartial() {
            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse result = new com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse(this);
            result.code_ = code_;
            result.message_ = message_;
            result.verifyCode_ = verifyCode_;
            result.expiresAt_ = expiresAt_;
            onBuilt();
            return result;
        }

        @java.lang.Override
        public Builder clone() {
            return super.clone();
        }

        @java.lang.Override
        public Builder setField(
                com.google.protobuf.Descriptors.FieldDescriptor field,
                java.lang.Object value) {
            return super.setField(field, value);
        }

        @java.lang.Override
        public Builder clearField(
                com.google.protobuf.Descriptors.FieldDescriptor field) {
            return super.clearField(field);
        }

        @java.lang.Override
        public Builder clearOneof(
                com.google.protobuf.Descriptors.OneofDescriptor oneof) {
            return super.clearOneof(oneof);
        }

        @java.lang.Override
        public Builder setRepeatedField(
                com.google.protobuf.Descriptors.FieldDescriptor field,
                int index, java.lang.Object value) {
            return super.setRepeatedField(field, index, value);
        }

        @java.lang.Override
        public Builder addRepeatedField(
                com.google.protobuf.Descriptors.FieldDescriptor field,
                java.lang.Object value) {
            return super.addRepeatedField(field, value);
        }

        @java.lang.Override
        public Builder mergeFrom(com.google.protobuf.Message other) {
            if (other instanceof com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse) {
                return mergeFrom((com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse) other);
            } else {
                super.mergeFrom(other);
                return this;
            }
        }

        public Builder mergeFrom(com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse other) {
            if (other == com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.getDefaultInstance())
                return this;
            if (other.getCode() != 0) {
                setCode(other.getCode());
            }
            if (!other.getMessage().isEmpty()) {
                message_ = other.message_;
                onChanged();
            }
            if (!other.getVerifyCode().isEmpty()) {
                verifyCode_ = other.verifyCode_;
                onChanged();
            }
            if (other.getExpiresAt() != 0L) {
                setExpiresAt(other.getExpiresAt());
            }
            this.mergeUnknownFields(other.unknownFields);
            onChanged();
            return this;
        }

        @java.lang.Override
        public final boolean isInitialized() {
            return true;
        }

        @java.lang.Override
        public Builder mergeFrom(
                com.google.protobuf.CodedInputStream input,
                com.google.protobuf.ExtensionRegistryLite extensionRegistry)
                throws java.io.IOException {
            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse parsedMessage = null;
            try {
                parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
            } catch (com.google.protobuf.InvalidProtocolBufferException e) {
                parsedMessage = (com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse) e.getUnfinishedMessage();
                throw e.unwrapIOException();
            } finally {
                if (parsedMessage != null) {
                    mergeFrom(parsedMessage);
                }
            }
            return this;
        }

        /**
         * <code>int32 code = 1;</code>
         */
        public int getCode() {
            return code_;
        }

        /**
         * <code>int32 code = 1;</code>
         */
        public Builder setCode(int value) {

            code_ = value;
            onChanged();
            return this;
        }

        /**
         * <code>int32 code = 1;</code>
         */
        public Builder clearCode() {

            code_ = 0;
            onChanged();
            return this;
        }

        /**
         * <code>string message = 2;</code>
         */
        public java.lang.String getMessage() {
            java.lang.Object ref = message_;
            if (!(ref instanceof java.lang.String)) {
                com.google.protobuf.ByteString bs =
                        (com.google.protobuf.ByteString) ref;
                java.lang.String s = bs.toStringUtf8();
                message_ = s;
                return s;
            } else {
                return (java.lang.String) ref;
            }
        }

        /**
         * <code>string message = 2;</code>
         */
        public Builder setMessage(
                java.lang.String value) {
            if (value == null) {
                throw new NullPointerException();
            }

            message_ = value;
            onChanged();
            return this;
        }

        /**
         * <code>string message = 2;</code>
         */
        public com.google.protobuf.ByteString
        getMessageBytes() {
            java.lang.Object ref = message_;
            if (ref instanceof String) {
                com.google.protobuf.ByteString b =
                        com.google.protobuf.ByteString.copyFromUtf8(
                                (java.lang.String) ref);
                message_ = b;
                return b;
            } else {
                return (com.google.protobuf.ByteString) ref;
            }
        }

        /**
         * <code>string message = 2;</code>
         */
        public Builder setMessageBytes(
                com.google.protobuf.ByteString value) {
            if (value == null) {
                throw new NullPointerException();
            }
            checkByteStringIsUtf8(value);

            message_ = value;
            onChanged();
            return this;
        }

        /**
         * <code>string message = 2;</code>
         */
        public Builder clearMessage() {

            message_ = getDefaultInstance().getMessage();
            onChanged();
            return this;
        }

        /**
         * <code>string verifyCode = 3;</code>
         */
        public java.lang.String getVerifyCode() {
            java.lang.Object ref = verifyCode_;
            if (!(ref instanceof java.lang.String)) {
                com.google.protobuf.ByteString bs =
                        (com.google.protobuf.ByteString) ref;
                java.lang.String s = bs.toStringUtf8();
                verifyCode_ = s;
                return s;
            } else {
                return (java.lang.String) ref;
            }
        }

        /**
         * <code>string verifyCode = 3;</code>
         */
        public Builder setVerifyCode(
                java.lang.String value) {
            if (value == null) {
                throw new NullPointerException();
            }

            verifyCode_ = value;
            onChanged();
            return this;
        }

        /**
         * <code>string verifyCode = 3;</code>
         */
        public com.google.protobuf.ByteString
        getVerifyCodeBytes() {
            java.lang.Object ref = verifyCode_;
            if (ref instanceof String) {
                com.google.protobuf.ByteString b =
                        com.google.protobuf.ByteString.copyFromUtf8(
                                (java.lang.String) ref);
                verifyCode_ = b;
                return b;
            } else {
                return (com.google.protobuf.ByteString) ref;
            }
        }

        /**
         * <code>string verifyCode = 3;</code>
         */
        public Builder setVerifyCodeBytes(
                com.google.protobuf.ByteString value) {
            if (value == null) {
                throw new NullPointerException();
            }
            checkByteStringIsUtf8(value);

            verifyCode_ = value;
            onChanged();
            return this;
        }

        /**
         * <code>string verifyCode = 3;</code>
         */
        public Builder clearVerifyCode() {

            verifyCode_ = getDefaultInstance().getVerifyCode();
            onChanged();
            return this;
        }

        /**
         * <code>int64 expiresAt = 4;</code>
         */
        public long getExpiresAt() {
            return expiresAt_;
        }

        /**
         * <code>int64 expiresAt = 4;</code>
         */
        public Builder setExpiresAt(long value) {

            expiresAt_ = value;
            onChanged();
            return this;
        }

        /**
         * <code>int64 expiresAt = 4;</code>
         */
        public Builder clearExpiresAt() {

            expiresAt_ = 0L;
            onChanged();
            return this;
        }

        @java.lang.Override
        public final Builder setUnknownFields(
                final com.google.protobuf.UnknownFieldSet unknownFields) {
            return super.setUnknownFieldsProto3(unknownFields);
        }

        @java.lang.Override
        public final Builder mergeUnknownFields(
                final com.google.protobuf.UnknownFieldSet unknownFields) {
            return super.mergeUnknownFields(unknownFields);
        }


        // @@protoc_insertion_point(builder_scope:notify.GenerateVerifyCodeResponse)
    }

}

