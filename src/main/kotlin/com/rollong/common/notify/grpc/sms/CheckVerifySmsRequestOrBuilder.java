// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: notify/sms.service.proto

package com.rollong.common.notify.grpc.sms;

public interface CheckVerifySmsRequestOrBuilder extends
        // @@protoc_insertion_point(interface_extends:notify.CheckVerifySmsRequest)
        com.google.protobuf.MessageOrBuilder {

    /**
     * <code>string mobile = 1;</code>
     */
    java.lang.String getMobile();

    /**
     * <code>string mobile = 1;</code>
     */
    com.google.protobuf.ByteString
    getMobileBytes();

    /**
     * <code>string action = 2;</code>
     */
    java.lang.String getAction();

    /**
     * <code>string action = 2;</code>
     */
    com.google.protobuf.ByteString
    getActionBytes();

    /**
     * <code>string verify_code = 3;</code>
     */
    java.lang.String getVerifyCode();

    /**
     * <code>string verify_code = 3;</code>
     */
    com.google.protobuf.ByteString
    getVerifyCodeBytes();

    /**
     * <code>string product = 4;</code>
     */
    java.lang.String getProduct();

    /**
     * <code>string product = 4;</code>
     */
    com.google.protobuf.ByteString
    getProductBytes();

    /**
     * <code>string client_id = 9;</code>
     */
    java.lang.String getClientId();

    /**
     * <code>string client_id = 9;</code>
     */
    com.google.protobuf.ByteString
    getClientIdBytes();

    /**
     * <code>string client_secret = 10;</code>
     */
    java.lang.String getClientSecret();

    /**
     * <code>string client_secret = 10;</code>
     */
    com.google.protobuf.ByteString
    getClientSecretBytes();
}
