package com.rollong.common.notify.grpc.push;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 *
 */
@javax.annotation.Generated(
        value = "by gRPC proto compiler (version 1.14.0)",
        comments = "Source: notify/push.service.proto")
public final class PushServiceGrpc {

    public static final String SERVICE_NAME = "notify.PushService";
    private static final int METHODID_BROADCAST = 0;
    private static final int METHODID_PUSH = 1;
    // Static method descriptors that strictly reflect the proto.
    private static volatile io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.push.BroadcastRequest,
            com.rollong.common.notify.grpc.push.PushResponse> getBroadcastMethod;
    private static volatile io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.push.PushRequest,
            com.rollong.common.notify.grpc.push.PushResponse> getPushMethod;
    private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

    private PushServiceGrpc() {
    }

    @io.grpc.stub.annotations.RpcMethod(
            fullMethodName = SERVICE_NAME + '/' + "Broadcast",
            requestType = com.rollong.common.notify.grpc.push.BroadcastRequest.class,
            responseType = com.rollong.common.notify.grpc.push.PushResponse.class,
            methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.push.BroadcastRequest,
            com.rollong.common.notify.grpc.push.PushResponse> getBroadcastMethod() {
        io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.push.BroadcastRequest, com.rollong.common.notify.grpc.push.PushResponse> getBroadcastMethod;
        if ((getBroadcastMethod = PushServiceGrpc.getBroadcastMethod) == null) {
            synchronized (PushServiceGrpc.class) {
                if ((getBroadcastMethod = PushServiceGrpc.getBroadcastMethod) == null) {
                    PushServiceGrpc.getBroadcastMethod = getBroadcastMethod =
                            io.grpc.MethodDescriptor.<com.rollong.common.notify.grpc.push.BroadcastRequest, com.rollong.common.notify.grpc.push.PushResponse>newBuilder()
                                    .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                                    .setFullMethodName(generateFullMethodName(
                                            "notify.PushService", "Broadcast"))
                                    .setSampledToLocalTracing(true)
                                    .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.push.BroadcastRequest.getDefaultInstance()))
                                    .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.push.PushResponse.getDefaultInstance()))
                                    .setSchemaDescriptor(new PushServiceMethodDescriptorSupplier("Broadcast"))
                                    .build();
                }
            }
        }
        return getBroadcastMethod;
    }

    @io.grpc.stub.annotations.RpcMethod(
            fullMethodName = SERVICE_NAME + '/' + "Push",
            requestType = com.rollong.common.notify.grpc.push.PushRequest.class,
            responseType = com.rollong.common.notify.grpc.push.PushResponse.class,
            methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.push.PushRequest,
            com.rollong.common.notify.grpc.push.PushResponse> getPushMethod() {
        io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.push.PushRequest, com.rollong.common.notify.grpc.push.PushResponse> getPushMethod;
        if ((getPushMethod = PushServiceGrpc.getPushMethod) == null) {
            synchronized (PushServiceGrpc.class) {
                if ((getPushMethod = PushServiceGrpc.getPushMethod) == null) {
                    PushServiceGrpc.getPushMethod = getPushMethod =
                            io.grpc.MethodDescriptor.<com.rollong.common.notify.grpc.push.PushRequest, com.rollong.common.notify.grpc.push.PushResponse>newBuilder()
                                    .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                                    .setFullMethodName(generateFullMethodName(
                                            "notify.PushService", "Push"))
                                    .setSampledToLocalTracing(true)
                                    .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.push.PushRequest.getDefaultInstance()))
                                    .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.push.PushResponse.getDefaultInstance()))
                                    .setSchemaDescriptor(new PushServiceMethodDescriptorSupplier("Push"))
                                    .build();
                }
            }
        }
        return getPushMethod;
    }

    /**
     * Creates a new async stub that supports all call types for the service
     */
    public static PushServiceStub newStub(io.grpc.Channel channel) {
        return new PushServiceStub(channel);
    }

    /**
     * Creates a new blocking-style stub that supports unary and streaming output calls on the service
     */
    public static PushServiceBlockingStub newBlockingStub(
            io.grpc.Channel channel) {
        return new PushServiceBlockingStub(channel);
    }

    /**
     * Creates a new ListenableFuture-style stub that supports unary calls on the service
     */
    public static PushServiceFutureStub newFutureStub(
            io.grpc.Channel channel) {
        return new PushServiceFutureStub(channel);
    }

    public static io.grpc.ServiceDescriptor getServiceDescriptor() {
        io.grpc.ServiceDescriptor result = serviceDescriptor;
        if (result == null) {
            synchronized (PushServiceGrpc.class) {
                result = serviceDescriptor;
                if (result == null) {
                    serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
                            .setSchemaDescriptor(new PushServiceFileDescriptorSupplier())
                            .addMethod(getBroadcastMethod())
                            .addMethod(getPushMethod())
                            .build();
                }
            }
        }
        return result;
    }

    /**
     *
     */
    public static abstract class PushServiceImplBase implements io.grpc.BindableService {

        /**
         *
         */
        public void broadcast(com.rollong.common.notify.grpc.push.BroadcastRequest request,
                              io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.push.PushResponse> responseObserver) {
            asyncUnimplementedUnaryCall(getBroadcastMethod(), responseObserver);
        }

        /**
         *
         */
        public void push(com.rollong.common.notify.grpc.push.PushRequest request,
                         io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.push.PushResponse> responseObserver) {
            asyncUnimplementedUnaryCall(getPushMethod(), responseObserver);
        }

        @java.lang.Override
        public final io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
                    .addMethod(
                            getBroadcastMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            com.rollong.common.notify.grpc.push.BroadcastRequest,
                                            com.rollong.common.notify.grpc.push.PushResponse>(
                                            this, METHODID_BROADCAST)))
                    .addMethod(
                            getPushMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            com.rollong.common.notify.grpc.push.PushRequest,
                                            com.rollong.common.notify.grpc.push.PushResponse>(
                                            this, METHODID_PUSH)))
                    .build();
        }
    }

    /**
     *
     */
    public static final class PushServiceStub extends io.grpc.stub.AbstractStub<PushServiceStub> {
        private PushServiceStub(io.grpc.Channel channel) {
            super(channel);
        }

        private PushServiceStub(io.grpc.Channel channel,
                                io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected PushServiceStub build(io.grpc.Channel channel,
                                        io.grpc.CallOptions callOptions) {
            return new PushServiceStub(channel, callOptions);
        }

        /**
         *
         */
        public void broadcast(com.rollong.common.notify.grpc.push.BroadcastRequest request,
                              io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.push.PushResponse> responseObserver) {
            asyncUnaryCall(
                    getChannel().newCall(getBroadcastMethod(), getCallOptions()), request, responseObserver);
        }

        /**
         *
         */
        public void push(com.rollong.common.notify.grpc.push.PushRequest request,
                         io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.push.PushResponse> responseObserver) {
            asyncUnaryCall(
                    getChannel().newCall(getPushMethod(), getCallOptions()), request, responseObserver);
        }
    }

    /**
     *
     */
    public static final class PushServiceBlockingStub extends io.grpc.stub.AbstractStub<PushServiceBlockingStub> {
        private PushServiceBlockingStub(io.grpc.Channel channel) {
            super(channel);
        }

        private PushServiceBlockingStub(io.grpc.Channel channel,
                                        io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected PushServiceBlockingStub build(io.grpc.Channel channel,
                                                io.grpc.CallOptions callOptions) {
            return new PushServiceBlockingStub(channel, callOptions);
        }

        /**
         *
         */
        public com.rollong.common.notify.grpc.push.PushResponse broadcast(com.rollong.common.notify.grpc.push.BroadcastRequest request) {
            return blockingUnaryCall(
                    getChannel(), getBroadcastMethod(), getCallOptions(), request);
        }

        /**
         *
         */
        public com.rollong.common.notify.grpc.push.PushResponse push(com.rollong.common.notify.grpc.push.PushRequest request) {
            return blockingUnaryCall(
                    getChannel(), getPushMethod(), getCallOptions(), request);
        }
    }

    /**
     *
     */
    public static final class PushServiceFutureStub extends io.grpc.stub.AbstractStub<PushServiceFutureStub> {
        private PushServiceFutureStub(io.grpc.Channel channel) {
            super(channel);
        }

        private PushServiceFutureStub(io.grpc.Channel channel,
                                      io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected PushServiceFutureStub build(io.grpc.Channel channel,
                                              io.grpc.CallOptions callOptions) {
            return new PushServiceFutureStub(channel, callOptions);
        }

        /**
         *
         */
        public com.google.common.util.concurrent.ListenableFuture<com.rollong.common.notify.grpc.push.PushResponse> broadcast(
                com.rollong.common.notify.grpc.push.BroadcastRequest request) {
            return futureUnaryCall(
                    getChannel().newCall(getBroadcastMethod(), getCallOptions()), request);
        }

        /**
         *
         */
        public com.google.common.util.concurrent.ListenableFuture<com.rollong.common.notify.grpc.push.PushResponse> push(
                com.rollong.common.notify.grpc.push.PushRequest request) {
            return futureUnaryCall(
                    getChannel().newCall(getPushMethod(), getCallOptions()), request);
        }
    }

    private static final class MethodHandlers<Req, Resp> implements
            io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
        private final PushServiceImplBase serviceImpl;
        private final int methodId;

        MethodHandlers(PushServiceImplBase serviceImpl, int methodId) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                case METHODID_BROADCAST:
                    serviceImpl.broadcast((com.rollong.common.notify.grpc.push.BroadcastRequest) request,
                            (io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.push.PushResponse>) responseObserver);
                    break;
                case METHODID_PUSH:
                    serviceImpl.push((com.rollong.common.notify.grpc.push.PushRequest) request,
                            (io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.push.PushResponse>) responseObserver);
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(
                io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                default:
                    throw new AssertionError();
            }
        }
    }

    private static abstract class PushServiceBaseDescriptorSupplier
            implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
        PushServiceBaseDescriptorSupplier() {
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
            return com.rollong.common.notify.grpc.push.PushServiceOuterClass.getDescriptor();
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
            return getFileDescriptor().findServiceByName("PushService");
        }
    }

    private static final class PushServiceFileDescriptorSupplier
            extends PushServiceBaseDescriptorSupplier {
        PushServiceFileDescriptorSupplier() {
        }
    }

    private static final class PushServiceMethodDescriptorSupplier
            extends PushServiceBaseDescriptorSupplier
            implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
        private final String methodName;

        PushServiceMethodDescriptorSupplier(String methodName) {
            this.methodName = methodName;
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
            return getServiceDescriptor().findMethodByName(methodName);
        }
    }
}
