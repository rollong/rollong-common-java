package com.rollong.common.notify.grpc.sms;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 *
 */
@javax.annotation.Generated(
        value = "by gRPC proto compiler (version 1.14.0)",
        comments = "Source: notify/sms.service.proto")
public final class SmsServiceGrpc {

    public static final String SERVICE_NAME = "notify.SmsService";
    private static final int METHODID_GENERATE_VERIFY_CODE = 0;
    private static final int METHODID_SEND_VERIFY_CODE = 1;
    private static final int METHODID_CHECK_VERIFY_CODE = 2;
    private static final int METHODID_SEND_TEMPLATE = 3;
    // Static method descriptors that strictly reflect the proto.
    private static volatile io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendVerifySmsRequest,
            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse> getGenerateVerifyCodeMethod;
    private static volatile io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendVerifySmsRequest,
            com.rollong.common.notify.grpc.sms.SendSmsResponse> getSendVerifyCodeMethod;
    private static volatile io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest,
            com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse> getCheckVerifyCodeMethod;
    private static volatile io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendTemplateRequest,
            com.rollong.common.notify.grpc.sms.SendTemplateResponse> getSendTemplateMethod;
    private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

    private SmsServiceGrpc() {
    }

    @io.grpc.stub.annotations.RpcMethod(
            fullMethodName = SERVICE_NAME + '/' + "GenerateVerifyCode",
            requestType = com.rollong.common.notify.grpc.sms.SendVerifySmsRequest.class,
            responseType = com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.class,
            methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendVerifySmsRequest,
            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse> getGenerateVerifyCodeMethod() {
        io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendVerifySmsRequest, com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse> getGenerateVerifyCodeMethod;
        if ((getGenerateVerifyCodeMethod = SmsServiceGrpc.getGenerateVerifyCodeMethod) == null) {
            synchronized (SmsServiceGrpc.class) {
                if ((getGenerateVerifyCodeMethod = SmsServiceGrpc.getGenerateVerifyCodeMethod) == null) {
                    SmsServiceGrpc.getGenerateVerifyCodeMethod = getGenerateVerifyCodeMethod =
                            io.grpc.MethodDescriptor.<com.rollong.common.notify.grpc.sms.SendVerifySmsRequest, com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse>newBuilder()
                                    .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                                    .setFullMethodName(generateFullMethodName(
                                            "notify.SmsService", "GenerateVerifyCode"))
                                    .setSampledToLocalTracing(true)
                                    .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.sms.SendVerifySmsRequest.getDefaultInstance()))
                                    .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse.getDefaultInstance()))
                                    .setSchemaDescriptor(new SmsServiceMethodDescriptorSupplier("GenerateVerifyCode"))
                                    .build();
                }
            }
        }
        return getGenerateVerifyCodeMethod;
    }

    @io.grpc.stub.annotations.RpcMethod(
            fullMethodName = SERVICE_NAME + '/' + "SendVerifyCode",
            requestType = com.rollong.common.notify.grpc.sms.SendVerifySmsRequest.class,
            responseType = com.rollong.common.notify.grpc.sms.SendSmsResponse.class,
            methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendVerifySmsRequest,
            com.rollong.common.notify.grpc.sms.SendSmsResponse> getSendVerifyCodeMethod() {
        io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendVerifySmsRequest, com.rollong.common.notify.grpc.sms.SendSmsResponse> getSendVerifyCodeMethod;
        if ((getSendVerifyCodeMethod = SmsServiceGrpc.getSendVerifyCodeMethod) == null) {
            synchronized (SmsServiceGrpc.class) {
                if ((getSendVerifyCodeMethod = SmsServiceGrpc.getSendVerifyCodeMethod) == null) {
                    SmsServiceGrpc.getSendVerifyCodeMethod = getSendVerifyCodeMethod =
                            io.grpc.MethodDescriptor.<com.rollong.common.notify.grpc.sms.SendVerifySmsRequest, com.rollong.common.notify.grpc.sms.SendSmsResponse>newBuilder()
                                    .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                                    .setFullMethodName(generateFullMethodName(
                                            "notify.SmsService", "SendVerifyCode"))
                                    .setSampledToLocalTracing(true)
                                    .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.sms.SendVerifySmsRequest.getDefaultInstance()))
                                    .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.sms.SendSmsResponse.getDefaultInstance()))
                                    .setSchemaDescriptor(new SmsServiceMethodDescriptorSupplier("SendVerifyCode"))
                                    .build();
                }
            }
        }
        return getSendVerifyCodeMethod;
    }

    @io.grpc.stub.annotations.RpcMethod(
            fullMethodName = SERVICE_NAME + '/' + "CheckVerifyCode",
            requestType = com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest.class,
            responseType = com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse.class,
            methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest,
            com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse> getCheckVerifyCodeMethod() {
        io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest, com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse> getCheckVerifyCodeMethod;
        if ((getCheckVerifyCodeMethod = SmsServiceGrpc.getCheckVerifyCodeMethod) == null) {
            synchronized (SmsServiceGrpc.class) {
                if ((getCheckVerifyCodeMethod = SmsServiceGrpc.getCheckVerifyCodeMethod) == null) {
                    SmsServiceGrpc.getCheckVerifyCodeMethod = getCheckVerifyCodeMethod =
                            io.grpc.MethodDescriptor.<com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest, com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse>newBuilder()
                                    .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                                    .setFullMethodName(generateFullMethodName(
                                            "notify.SmsService", "CheckVerifyCode"))
                                    .setSampledToLocalTracing(true)
                                    .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest.getDefaultInstance()))
                                    .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse.getDefaultInstance()))
                                    .setSchemaDescriptor(new SmsServiceMethodDescriptorSupplier("CheckVerifyCode"))
                                    .build();
                }
            }
        }
        return getCheckVerifyCodeMethod;
    }

    @io.grpc.stub.annotations.RpcMethod(
            fullMethodName = SERVICE_NAME + '/' + "SendTemplate",
            requestType = com.rollong.common.notify.grpc.sms.SendTemplateRequest.class,
            responseType = com.rollong.common.notify.grpc.sms.SendTemplateResponse.class,
            methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendTemplateRequest,
            com.rollong.common.notify.grpc.sms.SendTemplateResponse> getSendTemplateMethod() {
        io.grpc.MethodDescriptor<com.rollong.common.notify.grpc.sms.SendTemplateRequest, com.rollong.common.notify.grpc.sms.SendTemplateResponse> getSendTemplateMethod;
        if ((getSendTemplateMethod = SmsServiceGrpc.getSendTemplateMethod) == null) {
            synchronized (SmsServiceGrpc.class) {
                if ((getSendTemplateMethod = SmsServiceGrpc.getSendTemplateMethod) == null) {
                    SmsServiceGrpc.getSendTemplateMethod = getSendTemplateMethod =
                            io.grpc.MethodDescriptor.<com.rollong.common.notify.grpc.sms.SendTemplateRequest, com.rollong.common.notify.grpc.sms.SendTemplateResponse>newBuilder()
                                    .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                                    .setFullMethodName(generateFullMethodName(
                                            "notify.SmsService", "SendTemplate"))
                                    .setSampledToLocalTracing(true)
                                    .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.sms.SendTemplateRequest.getDefaultInstance()))
                                    .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                            com.rollong.common.notify.grpc.sms.SendTemplateResponse.getDefaultInstance()))
                                    .setSchemaDescriptor(new SmsServiceMethodDescriptorSupplier("SendTemplate"))
                                    .build();
                }
            }
        }
        return getSendTemplateMethod;
    }

    /**
     * Creates a new async stub that supports all call types for the service
     */
    public static SmsServiceStub newStub(io.grpc.Channel channel) {
        return new SmsServiceStub(channel);
    }

    /**
     * Creates a new blocking-style stub that supports unary and streaming output calls on the service
     */
    public static SmsServiceBlockingStub newBlockingStub(
            io.grpc.Channel channel) {
        return new SmsServiceBlockingStub(channel);
    }

    /**
     * Creates a new ListenableFuture-style stub that supports unary calls on the service
     */
    public static SmsServiceFutureStub newFutureStub(
            io.grpc.Channel channel) {
        return new SmsServiceFutureStub(channel);
    }

    public static io.grpc.ServiceDescriptor getServiceDescriptor() {
        io.grpc.ServiceDescriptor result = serviceDescriptor;
        if (result == null) {
            synchronized (SmsServiceGrpc.class) {
                result = serviceDescriptor;
                if (result == null) {
                    serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
                            .setSchemaDescriptor(new SmsServiceFileDescriptorSupplier())
                            .addMethod(getGenerateVerifyCodeMethod())
                            .addMethod(getSendVerifyCodeMethod())
                            .addMethod(getCheckVerifyCodeMethod())
                            .addMethod(getSendTemplateMethod())
                            .build();
                }
            }
        }
        return result;
    }

    /**
     *
     */
    public static abstract class SmsServiceImplBase implements io.grpc.BindableService {

        /**
         *
         */
        public void generateVerifyCode(com.rollong.common.notify.grpc.sms.SendVerifySmsRequest request,
                                       io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse> responseObserver) {
            asyncUnimplementedUnaryCall(getGenerateVerifyCodeMethod(), responseObserver);
        }

        /**
         *
         */
        public void sendVerifyCode(com.rollong.common.notify.grpc.sms.SendVerifySmsRequest request,
                                   io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.SendSmsResponse> responseObserver) {
            asyncUnimplementedUnaryCall(getSendVerifyCodeMethod(), responseObserver);
        }

        /**
         *
         */
        public void checkVerifyCode(com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest request,
                                    io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse> responseObserver) {
            asyncUnimplementedUnaryCall(getCheckVerifyCodeMethod(), responseObserver);
        }

        /**
         *
         */
        public void sendTemplate(com.rollong.common.notify.grpc.sms.SendTemplateRequest request,
                                 io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.SendTemplateResponse> responseObserver) {
            asyncUnimplementedUnaryCall(getSendTemplateMethod(), responseObserver);
        }

        @java.lang.Override
        public final io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
                    .addMethod(
                            getGenerateVerifyCodeMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            com.rollong.common.notify.grpc.sms.SendVerifySmsRequest,
                                            com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse>(
                                            this, METHODID_GENERATE_VERIFY_CODE)))
                    .addMethod(
                            getSendVerifyCodeMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            com.rollong.common.notify.grpc.sms.SendVerifySmsRequest,
                                            com.rollong.common.notify.grpc.sms.SendSmsResponse>(
                                            this, METHODID_SEND_VERIFY_CODE)))
                    .addMethod(
                            getCheckVerifyCodeMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest,
                                            com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse>(
                                            this, METHODID_CHECK_VERIFY_CODE)))
                    .addMethod(
                            getSendTemplateMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            com.rollong.common.notify.grpc.sms.SendTemplateRequest,
                                            com.rollong.common.notify.grpc.sms.SendTemplateResponse>(
                                            this, METHODID_SEND_TEMPLATE)))
                    .build();
        }
    }

    /**
     *
     */
    public static final class SmsServiceStub extends io.grpc.stub.AbstractStub<SmsServiceStub> {
        private SmsServiceStub(io.grpc.Channel channel) {
            super(channel);
        }

        private SmsServiceStub(io.grpc.Channel channel,
                               io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected SmsServiceStub build(io.grpc.Channel channel,
                                       io.grpc.CallOptions callOptions) {
            return new SmsServiceStub(channel, callOptions);
        }

        /**
         *
         */
        public void generateVerifyCode(com.rollong.common.notify.grpc.sms.SendVerifySmsRequest request,
                                       io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse> responseObserver) {
            asyncUnaryCall(
                    getChannel().newCall(getGenerateVerifyCodeMethod(), getCallOptions()), request, responseObserver);
        }

        /**
         *
         */
        public void sendVerifyCode(com.rollong.common.notify.grpc.sms.SendVerifySmsRequest request,
                                   io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.SendSmsResponse> responseObserver) {
            asyncUnaryCall(
                    getChannel().newCall(getSendVerifyCodeMethod(), getCallOptions()), request, responseObserver);
        }

        /**
         *
         */
        public void checkVerifyCode(com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest request,
                                    io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse> responseObserver) {
            asyncUnaryCall(
                    getChannel().newCall(getCheckVerifyCodeMethod(), getCallOptions()), request, responseObserver);
        }

        /**
         *
         */
        public void sendTemplate(com.rollong.common.notify.grpc.sms.SendTemplateRequest request,
                                 io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.SendTemplateResponse> responseObserver) {
            asyncUnaryCall(
                    getChannel().newCall(getSendTemplateMethod(), getCallOptions()), request, responseObserver);
        }
    }

    /**
     *
     */
    public static final class SmsServiceBlockingStub extends io.grpc.stub.AbstractStub<SmsServiceBlockingStub> {
        private SmsServiceBlockingStub(io.grpc.Channel channel) {
            super(channel);
        }

        private SmsServiceBlockingStub(io.grpc.Channel channel,
                                       io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected SmsServiceBlockingStub build(io.grpc.Channel channel,
                                               io.grpc.CallOptions callOptions) {
            return new SmsServiceBlockingStub(channel, callOptions);
        }

        /**
         *
         */
        public com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse generateVerifyCode(com.rollong.common.notify.grpc.sms.SendVerifySmsRequest request) {
            return blockingUnaryCall(
                    getChannel(), getGenerateVerifyCodeMethod(), getCallOptions(), request);
        }

        /**
         *
         */
        public com.rollong.common.notify.grpc.sms.SendSmsResponse sendVerifyCode(com.rollong.common.notify.grpc.sms.SendVerifySmsRequest request) {
            return blockingUnaryCall(
                    getChannel(), getSendVerifyCodeMethod(), getCallOptions(), request);
        }

        /**
         *
         */
        public com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse checkVerifyCode(com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest request) {
            return blockingUnaryCall(
                    getChannel(), getCheckVerifyCodeMethod(), getCallOptions(), request);
        }

        /**
         *
         */
        public com.rollong.common.notify.grpc.sms.SendTemplateResponse sendTemplate(com.rollong.common.notify.grpc.sms.SendTemplateRequest request) {
            return blockingUnaryCall(
                    getChannel(), getSendTemplateMethod(), getCallOptions(), request);
        }
    }

    /**
     *
     */
    public static final class SmsServiceFutureStub extends io.grpc.stub.AbstractStub<SmsServiceFutureStub> {
        private SmsServiceFutureStub(io.grpc.Channel channel) {
            super(channel);
        }

        private SmsServiceFutureStub(io.grpc.Channel channel,
                                     io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @java.lang.Override
        protected SmsServiceFutureStub build(io.grpc.Channel channel,
                                             io.grpc.CallOptions callOptions) {
            return new SmsServiceFutureStub(channel, callOptions);
        }

        /**
         *
         */
        public com.google.common.util.concurrent.ListenableFuture<com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse> generateVerifyCode(
                com.rollong.common.notify.grpc.sms.SendVerifySmsRequest request) {
            return futureUnaryCall(
                    getChannel().newCall(getGenerateVerifyCodeMethod(), getCallOptions()), request);
        }

        /**
         *
         */
        public com.google.common.util.concurrent.ListenableFuture<com.rollong.common.notify.grpc.sms.SendSmsResponse> sendVerifyCode(
                com.rollong.common.notify.grpc.sms.SendVerifySmsRequest request) {
            return futureUnaryCall(
                    getChannel().newCall(getSendVerifyCodeMethod(), getCallOptions()), request);
        }

        /**
         *
         */
        public com.google.common.util.concurrent.ListenableFuture<com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse> checkVerifyCode(
                com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest request) {
            return futureUnaryCall(
                    getChannel().newCall(getCheckVerifyCodeMethod(), getCallOptions()), request);
        }

        /**
         *
         */
        public com.google.common.util.concurrent.ListenableFuture<com.rollong.common.notify.grpc.sms.SendTemplateResponse> sendTemplate(
                com.rollong.common.notify.grpc.sms.SendTemplateRequest request) {
            return futureUnaryCall(
                    getChannel().newCall(getSendTemplateMethod(), getCallOptions()), request);
        }
    }

    private static final class MethodHandlers<Req, Resp> implements
            io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
        private final SmsServiceImplBase serviceImpl;
        private final int methodId;

        MethodHandlers(SmsServiceImplBase serviceImpl, int methodId) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                case METHODID_GENERATE_VERIFY_CODE:
                    serviceImpl.generateVerifyCode((com.rollong.common.notify.grpc.sms.SendVerifySmsRequest) request,
                            (io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.GenerateVerifyCodeResponse>) responseObserver);
                    break;
                case METHODID_SEND_VERIFY_CODE:
                    serviceImpl.sendVerifyCode((com.rollong.common.notify.grpc.sms.SendVerifySmsRequest) request,
                            (io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.SendSmsResponse>) responseObserver);
                    break;
                case METHODID_CHECK_VERIFY_CODE:
                    serviceImpl.checkVerifyCode((com.rollong.common.notify.grpc.sms.CheckVerifySmsRequest) request,
                            (io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.CheckVerifySmsResponse>) responseObserver);
                    break;
                case METHODID_SEND_TEMPLATE:
                    serviceImpl.sendTemplate((com.rollong.common.notify.grpc.sms.SendTemplateRequest) request,
                            (io.grpc.stub.StreamObserver<com.rollong.common.notify.grpc.sms.SendTemplateResponse>) responseObserver);
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(
                io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                default:
                    throw new AssertionError();
            }
        }
    }

    private static abstract class SmsServiceBaseDescriptorSupplier
            implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
        SmsServiceBaseDescriptorSupplier() {
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
            return com.rollong.common.notify.grpc.sms.SmsServiceOuterClass.getDescriptor();
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
            return getFileDescriptor().findServiceByName("SmsService");
        }
    }

    private static final class SmsServiceFileDescriptorSupplier
            extends SmsServiceBaseDescriptorSupplier {
        SmsServiceFileDescriptorSupplier() {
        }
    }

    private static final class SmsServiceMethodDescriptorSupplier
            extends SmsServiceBaseDescriptorSupplier
            implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
        private final String methodName;

        SmsServiceMethodDescriptorSupplier(String methodName) {
            this.methodName = methodName;
        }

        @java.lang.Override
        public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
            return getServiceDescriptor().findMethodByName(methodName);
        }
    }
}
