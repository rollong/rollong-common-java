package com.rollong.common.ioc

import org.springframework.beans.BeansException
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

fun <T> Class<T>.getApplicationBean() = ApplicationContextResolver.getApplicationContext()!!.getBean(this)

fun <T : Any> KClass<T>.getApplicationBean() = ApplicationContextResolver.getApplicationContext()!!.getBean(this.java)

@Component
class ApplicationContextResolver : ApplicationContextAware {

    @Throws(BeansException::class)
    override fun setApplicationContext(applicationContext: ApplicationContext) {
        context = applicationContext
    }

    companion object {
        private var context: ApplicationContext? = null
        fun getApplicationContext(): ApplicationContext? {
            return context
        }
    }
}

