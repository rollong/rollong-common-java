package com.rollong.common.ioc

import org.springframework.beans.BeansException
import org.springframework.context.EnvironmentAware
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component

@Component
class EnvironmentResolver : EnvironmentAware {

    @Throws(BeansException::class)
    override fun setEnvironment(environment: Environment) {
        EnvironmentResolver.environment = environment
    }

    companion object {
        private var environment: Environment? = null

        fun getEnvironment(): Environment? {
            return environment
        }

        fun acceptsProfile(profile: String): Boolean {
            return getEnvironment()!!.acceptsProfiles(profile)
        }

        fun acceptsAnyProfiles(vararg profiles: String): Boolean {
            return getEnvironment()!!.acceptsProfiles(*profiles)
        }

        fun acceptsAllProfiles(vararg profiles: String): Boolean {
            val environment = getEnvironment()!!
            for (profile in profiles) {
                if (!environment.acceptsProfiles(profile)) {
                    return false
                }
            }
            return true
        }

        fun acceptsNoneProfiles(vararg profiles: String): Boolean {
            val environment = getEnvironment()!!
            for (profile in profiles) {
                if (environment.acceptsProfiles(profile)) {
                    return false
                }
            }
            return true
        }
    }
}