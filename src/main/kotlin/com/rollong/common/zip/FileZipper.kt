package com.rollong.common.zip

import org.apache.tools.zip.ZipEntry
import org.apache.tools.zip.ZipFile
import org.apache.tools.zip.ZipOutputStream
import java.io.*


/**
 * 压缩或解压zip：
 * 由于直接使用java.util.zip工具包下的类，会出现中文乱码问题，所以使用ant.jar中的org.apache.tools.zip下的工具类
 */

object FileZipper {

    fun zipOutputStream(outputStream: OutputStream) = ZipOutputStream(outputStream)

    /**
     * 压缩文件或路径
     * @param zipOutputStream 目标zip的outputStream
     * @param src 压缩的源文件
     * @param encoding 编码格式
     */
    @Throws(FileNotFoundException::class, IOException::class)
    fun zipFile(zipOutputStream: ZipOutputStream, src: List<File>, encoding: String = "GBK") {
        zipOutputStream.encoding = encoding
        src.forEach {
            this.zip(zipOutputStream, it)
        }
        zipOutputStream.close()
    }

    @Throws(FileNotFoundException::class, IOException::class)
    private fun traverse(file: File, rootPath: String = "", action: (leaf: File, path: String) -> Any) {
        val path = rootPath.trim(File.separatorChar)
        if (file.isDirectory) {
            val children = file.listFiles()
            if (children.isEmpty()) {
                action.invoke(file, path + file.name)
            } else {
                children.map {
                    this.traverse(file = it, action = action, rootPath = path + File.separatorChar + file.name)
                }
            }
        } else {
            action.invoke(file, path + File.separatorChar + file.name)
        }
    }

    /**
     *
     * @param zipOutputStream
     * @param encoding 编码格式
     * @param srcFile  被压缩的文件信息
     * @param rootPath  在zip中的相对路径
     * @throws IOException
     * @throws FileNotFoundException
     */
    @Throws(FileNotFoundException::class, IOException::class)
    fun zip(zipOutputStream: ZipOutputStream, srcFile: File, encoding: String = "GBK") {
        zipOutputStream.encoding = encoding
        this.traverse(
                file = srcFile
        ) { file, path ->
            if (file.isDirectory) {
                zipOutputStream.putNextEntry(ZipEntry(path + File.separatorChar))
            } else {
                zipOutputStream.putNextEntry(ZipEntry(path))
                file.inputStream().copyTo(zipOutputStream)
            }
            zipOutputStream.closeEntry()
        }
        zipOutputStream.close()
    }

    @Throws(IOException::class)
    fun zip(zipOutputStream: ZipOutputStream, streams: Map<String, InputStream?>, encoding: String = "GBK") {
        zipOutputStream.encoding = encoding
        streams.forEach { t, u ->
            if (null == u) {
                zipOutputStream.putNextEntry(ZipEntry(t.trimEnd(File.separatorChar) + File.separatorChar))
            } else {
                zipOutputStream.putNextEntry(ZipEntry(t.trimEnd(File.separatorChar)))
                u.copyTo(zipOutputStream)
            }
            zipOutputStream.closeEntry()
        }
        zipOutputStream.close()
    }

    /**
     * 对.zip文件进行解压缩
     * @param zipFile  解压缩文件
     * @param descDir  压缩的目标地址，如：D:\\测试 或 /mnt/d/测试
     * @param encoding 编码格式
     * @return
     */
    @Throws(IOException::class)
    fun upzipFile(
            zipFile: File,
            descDir: String,
            encoding: String = "GBK"
    ): List<File> {
        val results = mutableListOf<File>()
        val sourceZip = ZipFile(zipFile, encoding)
        val entries = sourceZip.entries
        while (entries.hasMoreElements()) {
            val entry = entries.nextElement() as ZipEntry
            val file = File(descDir.trimEnd(File.separatorChar) + File.separatorChar + entry.name)
            if (entry.isDirectory) {
                file.mkdirs()
            } else {
                val parent = file.parentFile
                if (!parent.exists()) {
                    parent.mkdirs()
                }
                val out = FileOutputStream(file)
                sourceZip.getInputStream(entry).copyTo(out)
                out.flush()
                out.close()
                results.add(file)
            }
        }
        return results
    }

    @Throws(IOException::class)
    fun upzipFile(
            zipFile: File,
            encoding: String = "GBK",
            action: (filename: String, outputStream: OutputStream?) -> Any
    ) {
        val sourceZip = ZipFile(zipFile, encoding)
        val entries = sourceZip.entries
        while (entries.hasMoreElements()) {
            val entry = entries.nextElement() as ZipEntry
            if (entry.isDirectory) {
                action.invoke(entry.name, null)
            } else {
                val out = ByteArrayOutputStream()
                sourceZip.getInputStream(entry).copyTo(out)
                action.invoke(entry.name, out)
                out.flush()
                out.close()
            }
        }
    }
}