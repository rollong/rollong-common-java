package com.rollong.common.gRPC

import io.grpc.ManagedChannelBuilder
import io.grpc.netty.GrpcSslContexts
import io.grpc.netty.NettyChannelBuilder
import java.io.File
import java.io.InputStream

class ChannelBuilder {
    private var ssl: Boolean = false
    private var address: String = "127.0.0.1"
    private var port: Int = 6565
    private var serverCertChain: InputStream? = null
    private var clientKeyCertChain: InputStream? = null
    private var clientKey: InputStream? = null

    fun ssl(ssl: Boolean): ChannelBuilder {
        this.ssl = ssl
        return this
    }

    fun address(address: String): ChannelBuilder {
        this.address = address
        return this
    }

    fun port(port: Int): ChannelBuilder {
        this.port = port
        return this
    }

    fun serverCertChain(serverCertChain: InputStream): ChannelBuilder {
        this.serverCertChain = serverCertChain
        return this
    }

    fun serverCertChain(file: File): ChannelBuilder {
        this.serverCertChain = file.inputStream()
        return this
    }

    fun clientCert(clientKeyCertChain: InputStream, clientKey: InputStream): ChannelBuilder {
        this.clientKeyCertChain = clientKeyCertChain
        this.clientKey = clientKey
        return this
    }

    fun build() = if (this.ssl) {
        val sslContextBuilder = GrpcSslContexts.forClient()
        if (null != this.clientKey && null != this.clientKeyCertChain) {
            sslContextBuilder.keyManager(this.clientKeyCertChain, this.clientKey)
        }
        this.serverCertChain?.let {
            sslContextBuilder.trustManager(it)
        }
        NettyChannelBuilder.forAddress(this.address, this.port)
                .sslContext(sslContextBuilder.build())
                .build()
    } else {
        ManagedChannelBuilder.forAddress(this.address, this.port).usePlaintext().build()
    }
}