package com.rollong.common.jwt

import com.rollong.common.util.ago
import com.rollong.common.util.base64decode
import com.rollong.common.util.later
import com.rollong.common.util.minutes
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.joda.time.DateTime
import java.util.*
import java.util.concurrent.TimeUnit

class JwtHelper(
        private val config: JwtConfig,
        private val algorithm: SignatureAlgorithm = SignatureAlgorithm.HS512
) {

    private val signingKey = this.config.secret.base64decode()

    val expiresInMinutes: Long
        get() = this.config.expiresInMinutes

    val refreshExpiresInMinutes: Long?
        get() = this.config.refreshExpiresInMinutes

    companion object {
        const val CLAIM_KEY_USERNAME = "sub"
        const val CLAIM_KEY_USERTYPE = "user_type"
        const val CLAIM_KEY_SESSIONID = "session_id"
        const val CLAIM_KEY_SCOPE = "scope"
    }

    data class TokenContent(
            val jti: String,
            val userType: String,
            val scope: List<String>?,
            val userName: String,
            val sessionId: String,
            val issuedAt: Long,
            val issuer: String,
            val expiresAt: Long
    )

    fun parse(
            token: String
    ): TokenContent {
        val claims = try {
            Jwts.parser()
                    .setSigningKey(this.signingKey)
                    .parseClaimsJws(token)
                    .body
        } catch (e: JwtException) {
            throw RuntimeException("jwt解析失败 错误信息:${e.message}")
        }
        return TokenContent(
                jti = claims.id,
                userType = claims.get(CLAIM_KEY_USERTYPE) as String,
                userName = claims.get(CLAIM_KEY_USERNAME) as String,
                sessionId = claims.get(CLAIM_KEY_SESSIONID) as String,
                issuedAt = claims.issuedAt.time,
                issuer = claims.issuer,
                expiresAt = claims.expiration.time,
                scope = claims.get(CLAIM_KEY_SCOPE) as List<String>?
        )
    }

    data class JwtToken(
            val jti: String,
            val token: String,
            val expiresAt: Date
    )

    fun issue(
            userName: String,
            userType: String,
            scope: List<String>? = null,
            sessionId: String? = null,
            issuer: String = "default",
            extraClaims: Map<String, Any>? = null,
            expires: Long? = null,
            timeUnit: TimeUnit = TimeUnit.MINUTES
    ): JwtToken {
        val jti = UUID.randomUUID().toString().replace("-", "")
        val expiresAt = (expires ?: this.config.expiresInMinutes).later(timeUnit).toDate()
        val claims: MutableMap<String, Any?> = mutableMapOf(
                CLAIM_KEY_USERNAME to userName,
                CLAIM_KEY_USERTYPE to userType,
                CLAIM_KEY_SESSIONID to (sessionId ?: jti),
                CLAIM_KEY_SCOPE to scope
        )
        extraClaims?.let {
            claims.putAll(it)
        }
        val token = Jwts.builder()
                .setClaims(claims)
                .setId(jti)
                .setIssuedAt(DateTime().toDate())
                .setNotBefore(5.minutes().ago().toDate())
                .setExpiration(expiresAt)
                .setIssuer(issuer)
                .signWith(this.algorithm, this.signingKey)
                .compact()
        return JwtToken(
                jti = jti,
                token = token,
                expiresAt = expiresAt
        )
    }
}