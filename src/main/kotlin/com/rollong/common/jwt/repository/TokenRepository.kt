package com.rollong.common.jwt.repository

import com.rollong.common.jwt.entity.JwtAuthorizable
import com.rollong.common.jwt.entity.TokenSession
import java.util.*

interface TokenRepository {
    fun save(session: TokenSession)
    fun revoke(session: TokenSession)
    fun sessions(user: JwtAuthorizable): Set<TokenSession>
    fun findBySessionId(sessionId: String): Optional<TokenSession>
    fun newSession(): TokenSession
}