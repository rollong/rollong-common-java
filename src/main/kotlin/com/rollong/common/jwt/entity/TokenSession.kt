package com.rollong.common.jwt.entity

import java.io.Serializable
import java.util.*

interface TokenSession : Serializable {
    var sessionId: String
    var userId: String
    var userType: String
    var jti: String
    var ip: String
    var payload: String?
    var loginAt: Date
    var logoutAt: Date?
    var expiresAt: Date
    var lastActivity: Date
    var refreshToken: String?
    var refreshTokenExpiresAt: Date?
}