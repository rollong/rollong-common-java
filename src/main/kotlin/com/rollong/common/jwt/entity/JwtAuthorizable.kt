package com.rollong.common.jwt.entity

interface JwtAuthorizable {
    val jwtUserName: String
    val jwtUserType: String
}