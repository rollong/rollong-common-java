package com.rollong.common.jwt.service

import com.rollong.common.exception.BadRequestException
import com.rollong.common.exception.UnauthorizedException
import com.rollong.common.jwt.JwtConfig
import com.rollong.common.jwt.JwtHelper
import com.rollong.common.jwt.entity.JwtAuthorizable
import com.rollong.common.jwt.payload.AuthResult
import com.rollong.common.jwt.repository.TokenRepository
import com.rollong.common.util.later
import com.rollong.common.util.toRandomString
import java.sql.Timestamp
import java.util.*
import java.util.concurrent.TimeUnit

abstract class AbstractJwtAuthService(
        jwtConfig: JwtConfig
) {

    protected val jwtHelper = JwtHelper(config = jwtConfig)

    protected abstract val tokenRepo: TokenRepository

    abstract fun findUser(id: String, type: String): JwtAuthorizable

    private val charArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=".toCharArray()

    fun <User : JwtAuthorizable> login(
            user: User,
            scope: List<String>? = null,
            ip: String = "",
            expires: Long? = null,
            refreshExpires: Long? = this.jwtHelper.refreshExpiresInMinutes,
            timeUnit: TimeUnit = TimeUnit.MINUTES,
            payload: String? = null,
            sessionId: String? = null
    ): AuthResult<User> {
        val now = Timestamp(System.currentTimeMillis())
        val accessToken = this.jwtHelper.issue(
                userName = user.jwtUserName,
                userType = user.jwtUserType,
                scope = scope,
                expires = expires,
                timeUnit = timeUnit,
                sessionId = sessionId
        )
        val session = this.tokenRepo.newSession()
        session.sessionId = sessionId ?: accessToken.jti
        session.userId = user.jwtUserName
        session.userType = user.jwtUserType
        session.jti = accessToken.jti
        session.ip = ip
        session.loginAt = now
        session.expiresAt = Timestamp(accessToken.expiresAt.time)
        session.payload = payload
        session.lastActivity = now
        if (null != refreshExpires) {
            session.refreshToken = this.charArray.toRandomString(64)
            session.refreshTokenExpiresAt = refreshExpires.later(timeUnit).toDate()
        }
        this.tokenRepo.save(session)
        return AuthResult(
                user = user,
                sessionId = session.sessionId,
                sessionPayload = payload,
                scope = scope,
                accessToken = accessToken,
                refreshToken = session.refreshToken?.let {
                    AuthResult.RefreshToken(
                            token = it,
                            expiresAt = session.refreshTokenExpiresAt!!
                    )
                }
        )
    }

    fun auth(token: String): AuthResult<JwtAuthorizable> {
        val tokenContent = try {
            this.jwtHelper.parse(token = token)
        } catch (e: Throwable) {
            throw UnauthorizedException("token解析失败")
        }
        val session = this.tokenRepo.findBySessionId(sessionId = tokenContent.sessionId).orElseThrow { UnauthorizedException("token已经失效") }
        if (null != session.logoutAt) {
            throw UnauthorizedException("您已经退出登陆")
        }
        if (session.expiresAt.time < System.currentTimeMillis()) {
            throw UnauthorizedException("登陆信息已经过期")
        }
        if (session.userId != tokenContent.userName || session.userType != tokenContent.userType) {
            throw UnauthorizedException("token凭据有误")
        }
        val jwtUser = this.findUser(tokenContent.userName, tokenContent.userType)
        return AuthResult(
                user = jwtUser,
                sessionId = tokenContent.sessionId,
                sessionPayload = session.payload,
                scope = tokenContent.scope,
                accessToken = JwtHelper.JwtToken(
                        jti = tokenContent.jti,
                        token = token,
                        expiresAt = Date(tokenContent.expiresAt)
                ),
                refreshToken = null
        )
    }

    fun refresh(
            token: String,
            refreshToken: String,
            expires: Long = this.jwtHelper.expiresInMinutes,
            refreshExpires: Long? = this.jwtHelper.refreshExpiresInMinutes,
            timeUnit: TimeUnit = TimeUnit.MINUTES
    ): AuthResult<JwtAuthorizable> {
        val result = this.auth(token = token)
        val session = this.tokenRepo.findBySessionId(sessionId = result.sessionId).orElseThrow { BadRequestException("没有找到session") }
        session.refreshTokenExpiresAt?.let {
            if (it.time > System.currentTimeMillis()) {
                throw BadRequestException("refresh_token已过期")
            }
        }
        if (session.refreshToken != refreshToken) {
            throw BadRequestException("refresh_token错误")
        }
        val accessToken = this.jwtHelper.issue(
                userName = result.user.jwtUserName,
                userType = result.user.jwtUserType,
                scope = result.scope,
                expires = expires,
                timeUnit = timeUnit,
                sessionId = session.sessionId
        )
        session.refreshToken = null
        session.refreshTokenExpiresAt = null
        if (null != refreshExpires) {
            session.refreshToken = this.charArray.toRandomString(64)
            session.refreshTokenExpiresAt = refreshExpires.later(timeUnit).toDate()
        }
        this.tokenRepo.save(session)
        return AuthResult(
                user = result.user,
                sessionId = session.sessionId,
                sessionPayload = session.payload,
                scope = result.scope,
                accessToken = accessToken,
                refreshToken = session.refreshToken?.let {
                    AuthResult.RefreshToken(
                            token = it,
                            expiresAt = session.refreshTokenExpiresAt!!
                    )
                }
        )
    }

    fun logout(token: String): AuthResult<JwtAuthorizable> {
        val authResult = this.auth(token = token)
        val session = this.tokenRepo.findBySessionId(sessionId = authResult.sessionId).orElse(null)
        session?.let {
            it.logoutAt = Timestamp(System.currentTimeMillis())
            this.tokenRepo.revoke(it)
        }
        return authResult
    }
}