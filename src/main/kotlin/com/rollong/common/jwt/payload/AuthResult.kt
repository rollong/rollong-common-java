package com.rollong.common.jwt.payload

import com.rollong.common.jwt.JwtHelper
import com.rollong.common.jwt.entity.JwtAuthorizable
import java.util.*

data class AuthResult<User : JwtAuthorizable>(
        val user: User,
        val scope: List<String>?,
        val sessionId: String,
        val accessToken: JwtHelper.JwtToken?,
        val refreshToken: RefreshToken?,
        val sessionPayload: String?
) {
    data class RefreshToken(
            val token: String,
            val expiresAt: Date
    )
}