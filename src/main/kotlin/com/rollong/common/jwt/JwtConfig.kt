package com.rollong.common.jwt

data class JwtConfig(
        var secret: String = "secret",
        var expiresInMinutes: Long = 43200,
        var refreshExpiresInMinutes: Long? = null
)