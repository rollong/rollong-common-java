package com.rollong.common.auth

interface HasPassword {
    var password: String
    var rawPassword: String
    fun checkPassword(rawPassword: String): Boolean
}