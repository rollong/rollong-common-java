package com.rollong.common.event

import com.rollong.common.ioc.ApplicationContextResolver
import org.springframework.context.ApplicationEvent

abstract class Event(
        source: Any
) : ApplicationEvent(
        source
) {
    fun publish() {
        ApplicationContextResolver.getApplicationContext()!!.publishEvent(this)
    }
}