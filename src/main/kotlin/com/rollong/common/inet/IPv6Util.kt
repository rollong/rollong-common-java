package com.rollong.common.inet

import java.math.BigInteger
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.regex.Pattern
import kotlin.experimental.and

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/10/28 16:19
 * @project 诺朗java-common库
 * @filename IPv6Util.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.inet
 * @description
 */
object IPv6Util {

    val pattern = Pattern.compile("^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:)|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}(:[0-9A-Fa-f]{1,4}){1,2})|(([0-9A-Fa-f]{1,4}:){4}(:[0-9A-Fa-f]{1,4}){1,3})|(([0-9A-Fa-f]{1,4}:){3}(:[0-9A-Fa-f]{1,4}){1,4})|(([0-9A-Fa-f]{1,4}:){2}(:[0-9A-Fa-f]{1,4}){1,5})|([0-9A-Fa-f]{1,4}:(:[0-9A-Fa-f]{1,4}){1,6})|(:(:[0-9A-Fa-f]{1,4}){1,7})|(([0-9A-Fa-f]{1,4}:){6}(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3})|(([0-9A-Fa-f]{1,4}:){5}:(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3})|(([0-9A-Fa-f]{1,4}:){4}(:[0-9A-Fa-f]{1,4}){0,1}:(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3})|(([0-9A-Fa-f]{1,4}:){3}(:[0-9A-Fa-f]{1,4}){0,2}:(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3})|(([0-9A-Fa-f]{1,4}:){2}(:[0-9A-Fa-f]{1,4}){0,3}:(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3})|([0-9A-Fa-f]{1,4}:(:[0-9A-Fa-f]{1,4}){0,4}:(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3})|(:(:[0-9A-Fa-f]{1,4}){0,5}:(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}))$")

    fun ipv6ToBigInteger(ipv6: String): BigInteger {
        var ipv6String = ipv6.trimStart(':')
        val ret = ByteArray(17)
        ret[0] = 0
        var ib = 16
        var comFlag = false// ipv4混合模式标记
        if (ipv6String.endsWith("::")) {
            ipv6String = ipv6String + "0"
        }
        val groups = ipv6String.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (ig in groups.size - 1 downTo -1 + 1) {// 反向扫描
            if (groups[ig].contains(".")) {
                // 出现ipv4混合模式
                val temp = ipv4ToBytes(groups[ig])
                ret[ib--] = temp[4]
                ret[ib--] = temp[3]
                ret[ib--] = temp[2]
                ret[ib--] = temp[1]
                comFlag = true
            } else if ("" == groups[ig]) {
                // 出现零长度压缩,计算缺少的组数
                var zlg = 9 - (groups.size + if (comFlag) 1 else 0)
                while (zlg-- > 0) {// 将这些组置0
                    ret[ib--] = 0
                    ret[ib--] = 0
                }
            } else {
                val temp = Integer.parseInt(groups[ig], 16)
                ret[ib--] = temp.toByte()
                ret[ib--] = (temp shr 8).toByte()
            }
        }
        return BigInteger(ret)
    }

    /**
     * 将整数形式的ipv6地址转换为字符串形式
     *
     */
    fun bigIntegerToIPv6(big: BigInteger): String {
        var big = big
        var str = ""
        val ff = BigInteger.valueOf(0xffff)
        for (i in 0..7) {
            str = big.and(ff).toString(16) + ":" + str
            big = big.shiftRight(16)
        }
        str = str.substring(0, str.length - 1)
        return str.replaceFirst("(^|:)(0+(:|$)){2,8}".toRegex(), "::")
    }

    fun isValidIPv6(ip: String) = pattern.matcher(ip).matches()

    private fun ipv4ToBytes(ipv4: String): ByteArray {
        val ret = ByteArray(5)
        ret[0] = 0
        // 先找到IP地址字符串中.的位置
        val position1 = ipv4.indexOf(".")
        val position2 = ipv4.indexOf(".", position1 + 1)
        val position3 = ipv4.indexOf(".", position2 + 1)
        // 将每个.之间的字符串转换成整型
        ret[1] = Integer.parseInt(ipv4.substring(0, position1)).toByte()
        ret[2] = Integer.parseInt(ipv4.substring(position1 + 1,
                position2)).toByte()
        ret[3] = Integer.parseInt(ipv4.substring(position2 + 1,
                position3)).toByte()
        ret[4] = Integer.parseInt(ipv4.substring(position3 + 1)).toByte()
        return ret
    }

    fun isInRange(ip: String, cidr: String): Boolean {
        val ipAddr = ipv6ToBigInteger(ip)
        val cidrIpAddr = ipv6ToBigInteger(cidr.split('/')[0])
        val length = cidr.split('/')[1].toInt()
        val mask = -0x1.toBigInteger().shiftLeft(128 - length)
        return ipAddr and mask == cidrIpAddr and mask
    }

    /**
     * 将ipv6每段补齐4位
     *
     * @return
     * @throws UnknownHostException
     */
    @Throws(UnknownHostException::class)
    fun fullIPv6(ipv6: String): String {
        val str = StringBuffer()
        if (isValidIPv6(ipv6)) {
            val ip = InetAddress.getByName(ipv6).toString().replace("/", "")
            val info = ip.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (i in info.indices) {
                when (info[i].length) {
                    1 -> info[i] = "000" + info[i]
                    2 -> info[i] = "00" + info[i]
                    3 -> info[i] = "0" + info[i]
                    else -> {
                    }
                }
                if (i < 7) {
                    str.append(info[i] + ":")
                } else {
                    str.append(info[i])
                }
            }
        }
        return str.toString()
    }

    /**
     * IPv4转换为IPv6
     */
    fun ipv4ToIpv6(ipv4: String): String {
        val octets = ipv4.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val octetBytes = ByteArray(4)
        for (i in 0..3) {
            octetBytes[i] = Integer.parseInt(octets[i]).toByte()
        }
        val ipv4asIpV6addr = ByteArray(16)
        ipv4asIpV6addr[10] = 0xff.toByte()
        ipv4asIpV6addr[11] = 0xff.toByte()
        ipv4asIpV6addr[12] = octetBytes[0]
        ipv4asIpV6addr[13] = octetBytes[1]
        ipv4asIpV6addr[14] = octetBytes[2]
        ipv4asIpV6addr[15] = octetBytes[3]
        return ("::" + (ipv4asIpV6addr[12] and 0xff.toByte()) + "." + (ipv4asIpV6addr[13] and 0xff.toByte()) + "."
                + (ipv4asIpV6addr[14] and 0xff.toByte()) + "." + (ipv4asIpV6addr[15] and 0xff.toByte()))
    }
}