package com.rollong.common.payment

class PaymentClientConfig(
        var partnerNo: String? = null,
        var login: String? = null,
        var password: String? = null,
        var host: String? = null
)