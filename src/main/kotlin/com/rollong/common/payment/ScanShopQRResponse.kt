package com.rollong.common.payment

class ScanShopQRResponse(
        var qrCodeUrl: String? = null
) : PayResponse()