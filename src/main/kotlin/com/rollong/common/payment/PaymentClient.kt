package com.rollong.common.payment

import com.alibaba.fastjson.JSON
import com.rollong.common.exception.APIRuntimeException
import com.rollong.common.exception.BadRequestException
import com.rollong.common.util.readAllToByteArray
import org.apache.http.client.methods.RequestBuilder
import org.apache.http.entity.ContentType
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.slf4j.LoggerFactory

class PaymentClient(
        private val appVersion: String,
        private val appName: String,
        private val config: PaymentClientConfig
) {

    private val logger = LoggerFactory.getLogger(PaymentClient::class.java)

    private var token: LoginResp.AccessToken? = null

    class WhoAmIResp(
            var code: Int? = null,
            var message: String? = null
    )

    fun healthCheck() {
        if (null != this.token) {
            try {
                val client = HttpClients.createDefault()
                val reqBody = mapOf(
                        "login" to this.config.login
                )
                val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
                val request = RequestBuilder
                        .post()
                        .setUri("${this.config.host}client/auth/whoami")
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", this.getToken())
                        .setEntity(entity)
                        .build()
                val response = client.execute(request)
                val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
                val resp = JSON.parseObject(respBody, WhoAmIResp::class.java)
                if (0 == resp.code) {
                    this.logger.info("whoami passed")
                }
            } catch (e: Throwable) {
                this.destroy()
                this.logger.error("error when whoami", this.config.partnerNo, this.config.login)
                this.logger.error("", e)
            }
        }
    }

    private fun getToken(): String {
        if (null == this.token?.accessToken || System.currentTimeMillis() >= (this.token?.expiresAt ?: 0L) - 120000) {
            this.token = this.login(
                    partnerNo = this.config.partnerNo!!,
                    login = this.config.login!!,
                    password = this.config.password!!
            )
        }
        return "Bearer ${this.token?.accessToken}"
    }

    fun destroy() {
        val token = this.token?.accessToken
        if (null != token && System.currentTimeMillis() < this.token?.expiresAt ?: Long.MAX_VALUE) {
            try {
                val client = HttpClients.createDefault()
                val reqBody = mapOf(
                        "login" to this.config.login
                )
                val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
                val request = RequestBuilder
                        .post()
                        .setUri("${this.config.host}client/auth/logout")
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", this.getToken())
                        .setEntity(entity)
                        .build()
                client.execute(request)
                this.token = null
                this.logger.info("destroyed")
            } catch (e: Throwable) {
                this.logger.error("ACCESS_TOKEN destory error with partnerId:{},login:{}", this.config.partnerNo, this.config.login)
                this.logger.error("", e)
            }
        }
    }

    class LoginResp(
            var code: Int? = null,
            var message: String? = null,
            var items: TokenItem? = null

    ) {
        class TokenItem(
                var token: AccessToken? = null
        )

        class AccessToken(
                var accessToken: String? = null,
                var expiresAt: Long? = null
        )
    }

    private fun login(
            partnerNo: String,
            login: String,
            password: String
    ): LoginResp.AccessToken {
        val client = HttpClients.createDefault()
        val reqBody = mapOf(
                "login" to login,
                "partnerNo" to partnerNo,
                "password" to password
        )
        val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
        val request = RequestBuilder
                .post()
                .setUri("${this.config.host}client/auth/appLogin")
                .addHeader("Content-Type", "application/json")
                .addHeader("App-Name", this.appName)
                .addHeader("App-Version", this.appVersion)
                .setEntity(entity)
                .build()
        val response = client.execute(request)
        try {
            val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
            val resp = try {
                JSON.parseObject(respBody, LoginResp::class.java)
            } catch (e: Throwable) {
                null
            }
            resp?.let {
                if (0 == resp.code) {
                    return resp.items?.token!!
                } else {
                    val e = APIRuntimeException(resp.message ?: "支付API返回code!=0")
                    e.errCode = resp.code ?: 500
                    throw e
                }
            }
            throw APIRuntimeException("无法解析支付API返回$respBody")
        } catch (e: Throwable) {
            throw e
        } finally {
            response.close()
            client.close()
        }
    }

    class WxJsAPIPayResp(
            var code: Int? = null,
            var message: String? = null,
            var items: WxJSAPIPayResponse? = null
    )

    fun wxJsApiPay(
            attach: String,
            body: String,
            detail: String,
            deviceInfo: String,
            expireInMinutes: Int,
            noCredit: Boolean = false,
            openId: String,
            orderSn: String,
            totalFee: Int
    ): WxJSAPIPayResponse {
        val client = HttpClients.createDefault()
        val reqBody = mapOf(
                "attach" to attach,
                "body" to body,
                "detail" to detail,
                "deviceInfo" to deviceInfo,
                "expireInMinutes" to expireInMinutes,
                "noCredit" to noCredit,
                "subOpenId" to openId,
                "orderSn" to orderSn,
                "totalFee" to totalFee
        )
        val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
        val request = RequestBuilder
                .post()
                .setUri("${this.config.host}client/payment/wxJsApi")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", this.getToken())
                .setEntity(entity)
                .build()
        val response = client.execute(request)
        try {
            val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
            val resp = try {
                JSON.parseObject(respBody, WxJsAPIPayResp::class.java)
            } catch (e: Throwable) {
                null
            }
            resp?.let {
                if (0 == resp.code) {
                    val item = resp.items!!
                    item.orderSn = orderSn
                    return item
                } else {
                    val e = APIRuntimeException(resp.message ?: "支付API返回code!=0")
                    e.errCode = resp.code ?: 500
                    throw e
                }
            }
            throw APIRuntimeException("无法解析支付API返回$respBody")
        } catch (e: Throwable) {
            throw e
        } finally {
            response.close()
            client.close()
        }
    }

    class QueryResp(
            var code: Int? = null,
            var message: String? = null,
            var items: PayResponse? = null
    )

    fun query(
            orderSn: String,
            waitInSeconds: Int = 6
    ): Boolean {
        val client = HttpClients.createDefault()
        val request = RequestBuilder
                .get()
                .setUri("${this.config.host}client/payment/query")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", this.getToken())
                .addParameter("orderSn", orderSn)
                .addParameter("waitInSeconds", waitInSeconds.toString())
                .build()
        val response = client.execute(request)
        try {
            val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
            val resp = try {
                JSON.parseObject(respBody, QueryResp::class.java)
            } catch (e: Throwable) {
                null
            }
            resp?.let {
                if (0 == resp.code) {
                    return resp.items!!.paySuccess
                } else {
                    val e = APIRuntimeException(resp.message ?: "支付API返回code!=0")
                    e.errCode = resp.code ?: 500
                    throw e
                }
            }
            throw APIRuntimeException("无法解析支付API返回$respBody")
        } catch (e: Throwable) {
            throw e
        } finally {
            response.close()
            client.close()
        }
    }

    class RefundResp(
            var code: Int? = null,
            var message: String? = null,
            var items: RefundResponse? = null
    )

    fun refund(
            outRefundNo: String? = null,
            orderSn: String? = null,
            outTradeNo: String? = null,
            refundFee: Int,
            refundReason: String
    ): Boolean {
        val client = HttpClients.createDefault()
        val request = RequestBuilder
                .post()
                .setUri("${this.config.host}client/payment/refund")
                .addHeader("Authorization", this.getToken())
                .addParameter("refundFee", refundFee.toString())
                .addParameter("refundReason", refundReason)
                .addParameter("outRefundNo", outRefundNo ?: "refund-0-$orderSn")
        if (null != outTradeNo) {
            request.addParameter("outTradeNo", outTradeNo)
        } else if (null != orderSn) {
            request.addParameter("orderSn", orderSn)
        } else {
            throw BadRequestException("outTradeNo/orderSn不能同时为空")
        }
        val response = client.execute(request.build())
        try {
            val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
            val resp = try {
                JSON.parseObject(respBody, RefundResp::class.java)
            } catch (e: Throwable) {
                null
            }
            resp?.let {
                if (0 == resp.code) {
                    val item = resp.items!!
                    return item.success ?: throw BadRequestException("支付接口返回无法解析")
                } else {
                    val e = APIRuntimeException(resp.message ?: "支付API返回code!=0")
                    e.errCode = resp.code ?: 500
                    throw e
                }
            }
            throw APIRuntimeException("无法解析支付API返回$respBody")
        } catch (e: Throwable) {
            throw e
        } finally {
            response.close()
            client.close()
        }
    }

    class ScanShopQRResp(
            var code: Int? = null,
            var message: String? = null,
            var items: ScanShopQRResponse? = null
    )

    fun scanShopQR(
            attach: String,
            body: String,
            detail: String,
            deviceInfo: String,
            expireInMinutes: Int,
            noCredit: Boolean = false,
            platform: String = "Wxpay",
            orderSn: String,
            totalFee: Int,
            productId: String = "1"
    ): ScanShopQRResponse {
        val client = HttpClients.createDefault()
        val reqBody = mapOf(
                "attach" to attach,
                "body" to body,
                "detail" to detail,
                "deviceInfo" to deviceInfo,
                "expireInMinutes" to expireInMinutes,
                "noCredit" to noCredit,
                "platform" to platform,
                "orderSn" to orderSn,
                "totalFee" to totalFee,
                "productId" to productId
        )
        val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
        val request = RequestBuilder
                .post()
                .setUri("${this.config.host}client/payment/scanShopQR")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", this.getToken())
                .setEntity(entity)
                .build()
        val response = client.execute(request)
        try {
            val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
            val resp = try {
                JSON.parseObject(respBody, ScanShopQRResp::class.java)
            } catch (e: Throwable) {
                null
            }
            resp?.let {
                if (0 == resp.code) {
                    val item = resp.items!!
                    item.orderSn = orderSn
                    return item
                } else {
                    val e = APIRuntimeException(resp.message ?: "支付API返回code!=0")
                    e.errCode = resp.code ?: 500
                    throw e
                }
            }
            throw APIRuntimeException("无法解析支付API返回$respBody")
        } catch (e: Throwable) {
            throw e
        } finally {
            response.close()
            client.close()
        }
    }

    class WxpaySendHBResp(
            var code: Int? = null,
            var message: String? = null,
            var items: WxpaySendHBResponse? = null
    )

    fun wxSendMiniProgramHb(
            billno: String,
            sendName: String,
            openId: String,
            amount: Int,
            totalNum: Int,
            wishing: String = "",
            actName: String = "",
            remark: String = "",
            sceneId: String = "PRODUCT_5"
    ): WxpaySendHBResponse {
        val client = HttpClients.createDefault()
        val reqBody = mapOf(
                "billno" to billno,
                "sendName" to sendName,
                "openId" to openId,
                "amount" to amount,
                "totalNum" to totalNum,
                "wishing" to wishing,
                "actName" to actName,
                "remark" to remark,
                "sceneId" to sceneId
        )
        val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
        val request = RequestBuilder
                .post()
                .setUri("${this.config.host}client/payment/wx/sendMiniProgramHb")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", this.getToken())
                .setEntity(entity)
                .build()
        val response = client.execute(request)
        try {
            val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
            val resp = try {
                JSON.parseObject(respBody, WxpaySendHBResp::class.java)
            } catch (e: Throwable) {
                null
            }
            resp?.let {
                if (0 == resp.code) {
                    val item = resp.items!!
                    return item
                } else {
                    val e = APIRuntimeException(resp.message ?: "支付API返回code!=0")
                    e.errCode = resp.code ?: 500
                    throw e
                }
            }
            throw APIRuntimeException("无法解析支付API返回$respBody")
        } catch (e: Throwable) {
            throw e
        } finally {
            response.close()
            client.close()
        }
    }

    class WxpayGetHBResp(
            var code: Int? = null,
            var message: String? = null,
            var items: WxpayGetHBResponse? = null
    )

    fun wxGetHbInfo(
            billno: String
    ): WxpayGetHBResponse {
        val client = HttpClients.createDefault()
        val reqBody = mapOf(
                "billno" to billno
        )
        val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
        val request = RequestBuilder
                .post()
                .setUri("${this.config.host}client/payment/wx/getHBInfo")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", this.getToken())
                .setEntity(entity)
                .build()
        val response = client.execute(request)
        try {
            val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
            val resp = try {
                JSON.parseObject(respBody, WxpayGetHBResp::class.java)
            } catch (e: Throwable) {
                null
            }
            resp?.let {
                if (0 == resp.code) {
                    val item = resp.items!!
                    return item
                } else {
                    val e = APIRuntimeException(resp.message ?: "支付API返回code!=0")
                    e.errCode = resp.code ?: 500
                    throw e
                }
            }
            throw APIRuntimeException("无法解析支付API返回$respBody")
        } catch (e: Throwable) {
            throw e
        } finally {
            response.close()
            client.close()
        }
    }
}