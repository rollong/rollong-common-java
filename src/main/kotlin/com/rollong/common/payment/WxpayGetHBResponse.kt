package com.rollong.common.payment

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-09-17 12:37
 * @project common
 * @filename WxpayGetHBResponse.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.payment
 * @description
 */
data class WxpayGetHBResponse(
        var mchBillno: String? = null,
        var mchId: String? = null,
        var detailId: String? = null,
        var status: String? = null,
        var amount: String? = null,
        var reason: String? = null,
        var sendTime: String? = null,
        var refundTime: String? = null,
        var refundAmount: String? = null,
        var rcvTime: String? = null
)