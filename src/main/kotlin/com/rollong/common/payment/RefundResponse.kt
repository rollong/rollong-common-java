package com.rollong.common.payment

class RefundResponse(
        var outRefundNo: String? = null,
        var outTradeNo: String? = null,
        var refundFee: Int? = null,
        var requestCreated: Boolean? = null,
        var success: Boolean? = null
)