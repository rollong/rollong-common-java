package com.rollong.common.payment

import io.swagger.annotations.ApiModelProperty
import org.codehaus.jackson.annotate.JsonIgnore
import org.codehaus.jackson.annotate.JsonProperty

class WxJSAPIPayResponse(
        var timestamp: Long? = null,
        var nonceStr: String? = null,
        @get:JsonProperty("package")
        @get:ApiModelProperty(name = "package")
        var prepayId: String? = null,
        var signType: String? = null,
        var paySign: String? = null
) : PayResponse() {
    @get:JsonIgnore
    val trimPrepayId: String? = this.prepayId?.let {
        if (it.startsWith("prepay_id=")) {
            it.substring("prepay_id=".length)
        } else {
            it
        }
    }
}