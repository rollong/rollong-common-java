package com.rollong.common.payment

import org.codehaus.jackson.annotate.JsonProperty

/**
 * Copyright 成都诺朗科技有限公司
 * @author DELL
 * @createdAt 2020-09-17 12:32
 * @project common
 * @filename WxpaySendHBResponse.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.payment
 * @description
 */
data class WxpaySendHBResponse(
        var mchBillno: String? = null,
        var mchId: String? = null,
        var wxappid: String? = null,
        var openid: String? = null,
        var amount: String? = null,
        var sendListId: String? = null,
        @get:JsonProperty("package")
        @set:JsonProperty("package")
        var jsapiPackage: String? = null
)