package com.rollong.common.payment

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*

open class PayResponse {
    var paySuccess: Boolean = false
    @get:JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    var paidAt: Date? = null
    var outTradeNo: String? = null
    var orderSn: String? = null
    var transactionId: String? = null
    var totalFee: Int? = null
}