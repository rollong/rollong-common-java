package com.rollong.common.restful

import com.google.common.base.Predicate
import org.springframework.http.ResponseEntity
import org.springframework.web.context.request.async.DeferredResult
import springfox.documentation.builders.PathSelectors
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import java.util.*

abstract class AbstractSwaggerConfig {

    protected fun docket(title: String, version: String, desc: String, paths: Predicate<String>): Docket {
        val info = ApiInfo(
                title,
                desc,
                version,
                "NO terms of service",
                Contact("Rollong", "https://rollong.com", "contact@rollong.com"),
                "The Apache License, Version 2.0", //链接显示文字
                "http://www.apache.org/licenses/LICENSE-2.0.html", //网站链接
                listOf()
        )
        return Docket(DocumentationType.SWAGGER_2)
                .groupName(title)
                .genericModelSubstitutes(DeferredResult::class.java)
                .genericModelSubstitutes(ResponseEntity::class.java)
                .produces(object : HashSet<String>() {
                    init {
                        add("application/json")
                    }
                })
                .useDefaultResponseMessages(false)
                .forCodeGeneration(true)
                .pathMapping("/")// base，最终调用接口后会和paths拼接在一起
                .select()
                .paths(paths)//过滤的接口
                .build()
                .securitySchemes(listOf(this.securityScheme()))
                .securityContexts(listOf(this.securityContext()))
                .apiInfo(info)
    }

    private fun securityScheme() = ApiKey(AbstractAPIController.TOKEN_HEADER, AbstractAPIController.TOKEN_HEADER, "header")

    private fun securityContext() = SecurityContext.builder()
            .securityReferences(listOf(this.defaultAuth()))
            .forPaths(PathSelectors.regex("^.*$"))
            .build()

    private fun defaultAuth() = SecurityReference(
            AbstractAPIController.TOKEN_HEADER,
            arrayOf(AuthorizationScope("global", "accessEverything"))
    )

}