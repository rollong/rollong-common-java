package com.rollong.common.restful

import com.rollong.common.exception.APIRuntimeException
import com.rollong.common.exception.BadRequestException
import com.rollong.common.exception.ExceptionReporter
import com.rollong.common.restful.response.GenericResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
abstract class AbstractAPIController {

    @Autowired
    private lateinit var exceptionReporter: ExceptionReporter

    @ExceptionHandler
    fun exceptionHandler(throwable: Throwable, request: HttpServletRequest, response: HttpServletResponse): GenericResponse<String> {
        this.exceptionReporter.report(throwable, request)
        return if (throwable is APIRuntimeException) {
            response.status = throwable.httpStatus.value()
            GenericResponse(
                    code = throwable.errCode,
                    message = throwable.message ?: "服务器内部错误"
            )
        } else {
            response.status = 422
            GenericResponse(
                    code = 422,
                    message = throwable.message ?: throwable.cause?.message ?: "服务器内部错误"
            )
        }
    }

    companion object {
        const val TOKEN_HEADER = "Authorization"
        const val TOKEN_STARTS = "Bearer "
    }

    protected fun trimToken(token: String) = when {
        !token.startsWith(TOKEN_STARTS) -> throw BadRequestException("token需要${TOKEN_STARTS}开头")
        token == TOKEN_STARTS -> throw BadRequestException("token为空")
        else -> token.substring(TOKEN_STARTS.length)
    }

    protected fun getToken(): String? {
        val servletRequestAttributes = RequestContextHolder.getRequestAttributes() as ServletRequestAttributes
        val request = servletRequestAttributes.request
        val token = request.getHeader(TOKEN_HEADER)
        return token?.let { this.trimToken(it) }
    }

}