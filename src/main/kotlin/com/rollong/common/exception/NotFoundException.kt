package com.rollong.common.exception

import org.springframework.http.HttpStatus

class NotFoundException(
        message: String
) : APIRuntimeException(
        errCode = 404,
        httpStatus = HttpStatus.NOT_FOUND,
        message = message
)