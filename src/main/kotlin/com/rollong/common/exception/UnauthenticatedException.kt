package com.rollong.common.exception

import org.springframework.http.HttpStatus

class UnauthenticatedException(
        message: String = "用户权限不足"
) : APIRuntimeException(
        errCode = 403,
        httpStatus = HttpStatus.FORBIDDEN,
        message = message
)