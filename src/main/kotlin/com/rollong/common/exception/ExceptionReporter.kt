package com.rollong.common.exception

import java.lang.reflect.Method
import javax.servlet.http.HttpServletRequest

interface ExceptionReporter {
    fun report(throwable: Throwable)
    fun report(throwable: Throwable, request: HttpServletRequest?)
    fun report(throwable: Throwable, method: Method?, vararg params: Any?)
}