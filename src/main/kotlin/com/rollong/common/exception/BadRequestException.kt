package com.rollong.common.exception

import org.springframework.http.HttpStatus

class BadRequestException(
        message: String = "请求参数不合法"
) : APIRuntimeException(
        errCode = 400,
        httpStatus = HttpStatus.BAD_REQUEST,
        message = message
)