package com.rollong.common.exception.bugsnag

import com.bugsnag.Bugsnag
import com.rollong.common.exception.ExceptionReporter
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.lang.reflect.Method
import javax.servlet.http.HttpServletRequest

@Service
class BugsnagReporter(
        private val client: Bugsnag
) : AsyncUncaughtExceptionHandler, ExceptionReporter {

    @Value("\${bugsnag.enabled}")
    private var enabled = false

    @Async
    override fun handleUncaughtException(ex: Throwable, method: Method, vararg params: Any) {
        if (this.enabled) {
            this.report(ex)
        }
    }

    @Async
    override fun report(throwable: Throwable) {
        throwable.printStackTrace()
        if (this.enabled) {
            val report = this.client.buildReport(throwable)
            this.client.notify(report)
        }
    }

    @Async
    override fun report(throwable: Throwable, request: HttpServletRequest?) {
        throwable.printStackTrace()
        if (this.enabled) {
            val report = this.client.buildReport(throwable)
            if (null != request) {
                report.addToTab("context", "data url", request.requestURI)
                report.addToTab("context", "data method", request.method)
                report.addToTab("context", "data parameters", request.parameterMap)
                report.addToTab("context", "data ip", request.remoteAddr)
                report.addToTab("context", "remote-host", request.remoteHost)
                report.addToTab("context", "os name", System.getProperty("os.name"))
                report.addToTab("context", "local-port", request.localPort)
                report.addToTab("context", "server-port", request.serverPort)
                report.addToTab("context", "remote-port", request.remotePort)
                report.addToTab("context", "charset", request.characterEncoding)
            }
            this.client.notify(report)
        }
    }

    @Async
    override fun report(throwable: Throwable, method: Method?, vararg params: Any?) {
        throwable.printStackTrace()
        if (this.enabled) {
            val report = this.client.buildReport(throwable)
            this.client.notify(report)
        }
    }
}