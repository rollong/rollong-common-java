package com.rollong.common.exception.bugsnag

import com.bugsnag.Bugsnag
import com.bugsnag.delivery.AsyncHttpDelivery
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BugsnagClient(
        private val bugsnagProperties: BugsnagProperties
) {

    @Value("\${info.build.version}")
    private var version: String? = null

    @Bean
    fun getBugsnag(): Bugsnag {
        val bugsnag = Bugsnag(this.bugsnagProperties.apiKey, this.bugsnagProperties.enabled)
        bugsnag.setAppVersion(this.version)
        bugsnag.setIgnoreClasses(*this.bugsnagProperties.ignoreClasses)
        bugsnag.delivery = AsyncHttpDelivery()
        return bugsnag
    }
}