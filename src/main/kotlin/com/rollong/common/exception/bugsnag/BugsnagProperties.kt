package com.rollong.common.exception.bugsnag

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "bugsnag")
class BugsnagProperties(
        var apiKey: String? = null,
        var ignoreClasses: Array<String> = arrayOf(),
        var enabled: Boolean = false
)