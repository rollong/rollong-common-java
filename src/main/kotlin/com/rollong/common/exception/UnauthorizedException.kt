package com.rollong.common.exception

import org.springframework.http.HttpStatus

class UnauthorizedException(
        message: String = "用户未登录"
) : APIRuntimeException(
        errCode = 401,
        httpStatus = HttpStatus.UNAUTHORIZED,
        message = message
)