package com.rollong.common.image

import java.awt.*
import java.awt.color.ColorSpace
import java.awt.image.*
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import javax.imageio.ImageIO

/**
 * link: https://yq.aliyun.com/ziliao/151407
 * author:lldy
 * time:2012-5-6下午6:37:18
 * 图片处理工具类：<br></br>
 * 功能：缩放图像、切割图像、图像类型转换、彩色转黑白、文字水印、图片水印等
 */
object ImageUtils {

    /**
     * 相对于图片的位置
     */
    val POSITION_UPPERLEFT = 0
    val POSITION_UPPERRIGHT = 10
    val POSITION_LOWERLEFT = 1
    val POSITION_LOWERRIGHT = 11
    /**
     * 几种常见的图片格式
     */
    val IMAGE_TYPE_GIF = "gif"// 图形交换格式
    val IMAGE_TYPE_JPG = "jpg"// 联合照片专家组
    val IMAGE_TYPE_JPEG = "jpeg"// 联合照片专家组
    val IMAGE_TYPE_BMP = "bmp"// 英文Bitmap（位图）的简写，它是Windows操作系统中的标准图像文件格式
    val IMAGE_TYPE_PNG = "png"// 可移植网络图形

    fun image2BufferedImage(image: Image): BufferedImage {
        val bufferedImage = BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB)
        val g = bufferedImage.createGraphics()
        g.drawImage(image, null, null)
        g.dispose()
        return bufferedImage
    }

    /**
     * 缩放并转换格式后保存
     *
     * @param input     源
     * @param output    目标
     * @param width：目标宽
     * @param height：目标高
     * @param format：文件格式
     * @return
     */
    fun scale(input: InputStream, output: OutputStream, width: Int, height: Int, format: String) {
        val src = ImageIO.read(input) // 读入文件
        val image = src.getScaledInstance(width, height, Image.SCALE_DEFAULT)
        val tag = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
        val g = tag.graphics
        g.drawImage(image, 0, 0, null) // 绘制缩小后的图
        g.dispose()
        ImageIO.write(tag, format, output)// 输出到文件流
    }

    /**
     * 缩放Image，此方法返回源图像按百分比缩放后的图像
     *
     * @param inputImage
     * @param percentage 百分比 允许的输入0<percentage></percentage><10000
     * @return
     */
    fun scaleByPercentage(inputImage: BufferedImage, percentage: Int): BufferedImage {
        //允许百分比
        if (0 > percentage || percentage > 10000) {
            throw ImagingOpException("Error::不合法的参数:percentage->$percentage,percentage应该大于0~小于10000")
        }
        //获取原始图像透明度类型
        val type = inputImage.colorModel.transparency
        //获取目标图像大小
        val w = inputImage.width * percentage
        val h = inputImage.height * percentage
        //开启抗锯齿
        val renderingHints = RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        //使用高质量压缩
        renderingHints[RenderingHints.KEY_RENDERING] = RenderingHints.VALUE_RENDER_QUALITY
        val img = BufferedImage(w, h, type)
        val graphics2d = img.createGraphics()
        graphics2d.setRenderingHints(renderingHints)
        graphics2d.drawImage(inputImage, 0, 0, w, h, 0, 0, inputImage
                .width, inputImage.height, null)
        graphics2d.dispose()
        return img
        /*此代码将返回Image类型
        return inputImage.getScaledInstance(inputImage.getWidth()*percentage,
                inputImage.getHeight()*percentage, Image.SCALE_SMOOTH);
        */
    }

    /**
     * 缩放Image，此方法返回源图像按给定最大宽度限制下按比例缩放后的图像
     *
     * @param inputImage
     * @param maxWidth：压缩后允许的最大宽度
     * @param maxHeight：压缩后允许的最大高度
     * @throws java.io.IOException return
     */
    @Throws(Exception::class)
    fun scaleByPixelRate(inputImage: BufferedImage, maxWidth: Int, maxHeight: Int): BufferedImage {
        //获取原始图像透明度类型
        val type = inputImage.colorModel.transparency
        val width = inputImage.width
        val height = inputImage.height
        var newWidth = maxWidth
        var newHeight = maxHeight
        //如果指定最大宽度超过比例
        if (width * maxHeight < height * maxWidth) {
            newWidth = newHeight * width / height
        }
        //如果指定最大高度超过比例
        if (width * maxHeight > height * maxWidth) {
            newHeight = newWidth * height / width
        }
        //开启抗锯齿
        val renderingHints = RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        //使用高质量压缩
        renderingHints[RenderingHints.KEY_RENDERING] = RenderingHints.VALUE_RENDER_QUALITY
        val img = BufferedImage(newWidth, newHeight, type)
        val graphics2d = img.createGraphics()
        graphics2d.setRenderingHints(renderingHints)
        graphics2d.drawImage(inputImage, 0, 0, newWidth, newHeight, 0, 0, width, height, null)
        graphics2d.dispose()
        return img
    }

    /**
     * 缩放Image，此方法返回源图像按给定宽度、高度限制下缩放后的图像
     *
     * @param inputImage
     * @param newWidth   压缩后宽度
     * @param newHeight  压缩后高度
     * @throws java.io.IOException return
     */
    @Throws(Exception::class)
    fun scaleByPixel(inputImage: BufferedImage, newWidth: Int, newHeight: Int): BufferedImage {
        //获取原始图像透明度类型
        val type = inputImage.colorModel.transparency
        val width = inputImage.width
        val height = inputImage.height
        //开启抗锯齿
        val renderingHints = RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        //使用高质量压缩
        renderingHints[RenderingHints.KEY_RENDERING] = RenderingHints.VALUE_RENDER_QUALITY
        val img = BufferedImage(newWidth, newHeight, type)
        val graphics2d = img.createGraphics()
        graphics2d.setRenderingHints(renderingHints)
        graphics2d.drawImage(inputImage, 0, 0, newWidth, newHeight, 0, 0, width, height, null)
        graphics2d.dispose()
        return img
    }

    /**
     * 切割图像，返回指定范围的图像
     *
     * @param inputImage
     * @param x          起点横坐标
     * @param y          起点纵坐标
     * @param width      切割图片宽度:如果宽度超出图片，将改为图片自x剩余宽度
     * @param height     切割图片高度：如果高度超出图片，将改为图片自y剩余高度
     * @param fill       指定目标图像大小超出时是否补白，如果true，则表示补白；false表示不补白，此时将重置目标图像大小
     * @return
     */
    fun cut(inputImage: BufferedImage, x: Int, y: Int, width: Int, height: Int, fill: Boolean): BufferedImage {
        var width = width
        var height = height
        //获取原始图像透明度类型
        val type = inputImage.colorModel.transparency
        val w = inputImage.width
        val h = inputImage.height
        var endx = x + width
        var endy = y + height
        if (x > w)
            throw ImagingOpException("起点横坐标超出源图像范围")
        if (y > h)
            throw ImagingOpException("起点纵坐标超出源图像范围")
        val img: BufferedImage
        //补白
        if (fill) {
            img = BufferedImage(width, height, type)
            //宽度超出限制
            if (w - x < width) {
                width = w - x
                endx = w
            }
            //高度超出限制
            if (h - y < height) {
                height = h - y
                endy = h
            }
            //不补
        } else {
            //宽度超出限制
            if (w - x < width) {
                width = w - x
                endx = w
            }
            //高度超出限制
            if (h - y < height) {
                height = h - y
                endy = h
            }
            img = BufferedImage(width, height, type)
        }
        //开启抗锯齿
        val renderingHints = RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        //使用高质量压缩
        renderingHints[RenderingHints.KEY_RENDERING] = RenderingHints.VALUE_RENDER_QUALITY
        val graphics2d = img.createGraphics()
        graphics2d.setRenderingHints(renderingHints)
        graphics2d.drawImage(inputImage, 0, 0, width, height, x, y, endx, endy, null)
        graphics2d.dispose()
        return img
    }

    /**
     * 切割图像，返回指定起点位置指定大小图像
     *
     * @param inputImage
     * @param startPoint 起点位置：左上：0、右上:10、左下:1、右下:11
     * @param width      切割图片宽度
     * @param height     切割图片高度
     * @param fill       指定目标图像大小超出时是否补白，如果true，则表示补白；false表示不补白，此时将重置目标图像大小
     * @return
     */
    fun cut(inputImage: BufferedImage, startPoint: Int, width: Int, height: Int, fill: Boolean): BufferedImage {
        var width = width
        var height = height
        //获取原始图像透明度类型
        val type = inputImage.colorModel.transparency
        val w = inputImage.width
        val h = inputImage.height
        val img: BufferedImage
        //补白
        if (fill) {
            img = BufferedImage(width, height, type)
            if (width > w)
                width = w
            if (height > h)
                height = h
            //不补
        } else {
            if (width > w)
                width = w
            if (height > h)
                height = h
            img = BufferedImage(width, height, type)
        }
        //开启抗锯齿
        val renderingHints = RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        //使用高质量压缩
        renderingHints[RenderingHints.KEY_RENDERING] = RenderingHints.VALUE_RENDER_QUALITY
        val graphics2d = img.createGraphics()
        graphics2d.setRenderingHints(renderingHints)
        when (startPoint) {
            //右上
            POSITION_UPPERRIGHT -> graphics2d.drawImage(inputImage, w - width, 0, w, height, 0, 0, width, height, null)
            //左下
            POSITION_LOWERLEFT -> graphics2d.drawImage(inputImage, 0, h - height, width, h, 0, 0, width, height, null)
            //右下
            POSITION_LOWERRIGHT -> graphics2d.drawImage(inputImage, w - width, h - height, w, h, 0, 0, width, height, null)
            //默认左上
            POSITION_UPPERLEFT -> graphics2d.drawImage(inputImage, 0, 0, width, height, 0, 0, width, height, null)
            else -> graphics2d.drawImage(inputImage, 0, 0, width, height, 0, 0, width, height, null)
        }
        graphics2d.dispose()
        return img
    }

    /**
     * 以指定角度旋转图片：使用正角度 theta 进行旋转，可将正 x 轴上的点转向正 y 轴。
     *
     * @param inputImage
     * @param degree     角度:以度数为单位
     * @return
     */
    fun rotateImage(inputImage: BufferedImage,
                    degree: Int): BufferedImage {
        val w = inputImage.width
        val h = inputImage.height
        val type = inputImage.colorModel.transparency
        val img = BufferedImage(w, h, type)
        val graphics2d = img.createGraphics()
        //开启抗锯齿
        val renderingHints = RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        //使用高质量压缩
        renderingHints[RenderingHints.KEY_RENDERING] = RenderingHints.VALUE_RENDER_QUALITY
        graphics2d.setRenderingHints(renderingHints)
        graphics2d.rotate(Math.toRadians(degree.toDouble()), (w / 2).toDouble(), (h / 2).toDouble())
        graphics2d.drawImage(inputImage, 0, 0, null)
        graphics2d.dispose()
        return img
    }

    /**
     * 水平翻转图像
     *
     * @param inputImage 目标图像
     * @return
     */
    fun flipHorizontalImage(inputImage: BufferedImage): BufferedImage {
        val w = inputImage.width
        val h = inputImage.height
        val img = BufferedImage(w, h, inputImage.colorModel.transparency)
        val graphics2d = img.createGraphics()
        graphics2d.drawImage(inputImage, 0, 0, w, h, w, 0, 0, h, null)
        graphics2d.dispose()
        return img
    }

    /**
     * 竖直翻转图像
     *
     * @param inputImage 目标图像
     * @return
     */
    fun flipVerticalImage(inputImage: BufferedImage): BufferedImage {
        val w = inputImage.width
        val h = inputImage.height
        val img = BufferedImage(w, h, inputImage.colorModel.transparency)
        val graphics2d = img.createGraphics()
        graphics2d.drawImage(inputImage, 0, 0, w, h, 0, h, w, 0, null)
        graphics2d.dispose()
        return img
    }

    /**
     * 图片水印
     *
     * @param inputImage 待处理图像
     * @param markImage  水印图像
     * @param x          水印位于图片左上角的 x 坐标值
     * @param y          水印位于图片左上角的 y 坐标值
     * @param alpha      水印透明度 0.1f ~ 1.0f
     */
    fun waterMark(inputImage: BufferedImage, markImage: BufferedImage, x: Int, y: Int,
                  alpha: Float): BufferedImage {
        val image = BufferedImage(inputImage.width, inputImage.height, BufferedImage.TYPE_INT_ARGB)
        val g = image.createGraphics()
        g.drawImage(inputImage, 0, 0, null)
        // 加载水印图像
        g.composite = AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                alpha)
        g.drawImage(markImage, x, y, null)
        g.dispose()
        return image
    }

    /**
     * 文字水印
     *
     * @param inputImage 待处理图像
     * @param text       水印文字
     * @param font       水印字体信息
     * @param color      水印字体颜色
     * @param x          水印位于图片左上角的 x 坐标值
     * @param y          水印位于图片左上角的 y 坐标值
     * @param alpha      水印透明度 0.1f ~ 1.0f
     */
    fun textMark(inputImage: BufferedImage, text: String, font: Font?, color: Color, x: Int, y: Int, alpha: Float): BufferedImage {
        val dfont = font ?: Font("宋体", 20, 13)
        val image = BufferedImage(inputImage.width, inputImage.height, BufferedImage.TYPE_INT_ARGB)
        val g = image.createGraphics()
        g.drawImage(inputImage, 0, 0, null)
        g.color = color
        g.font = dfont
        g.composite = AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha)
        g.drawString(text, x, y)
        g.dispose()
        return image
    }

    /**
     * 图像彩色转黑白色
     *
     * @param inputImage
     * @return 转换后的BufferedImage
     */
    fun toGray(inputImage: BufferedImage): BufferedImage {
        val cs = ColorSpace.getInstance(ColorSpace.CS_GRAY)
        //对源 BufferedImage 进行颜色转换。如果目标图像为 null，
        //则根据适当的 ColorModel 创建 BufferedImage。
        val op = ColorConvertOp(cs, null)
        return op.filter(inputImage, null)
    }

    /**
     * 图像彩色转为黑白
     *
     * @param input  源图像地址
     * @param output 目标图像地址
     * @param formatType    目标图像格式：如果formatType为null;将默认转换为PNG
     */
    fun toGray(input: InputStream, output: OutputStream, formatType: String?) {
        var src = ImageIO.read(input)
        val cs = ColorSpace.getInstance(ColorSpace.CS_GRAY)
        val op = ColorConvertOp(cs, null)
        src = op.filter(src, null)
        ImageIO.write(src, formatType ?: IMAGE_TYPE_PNG, output)
    }

    /**
     * 图像类型转换：GIF->JPG、GIF->PNG、PNG->JPG、PNG->GIF(X)、BMP->PNG
     *
     * @param inputImage    源图像地址
     * @param formatType    包含格式非正式名称的 String：如JPG、JPEG、GIF等
     * @param output 目标图像地址
     */
    fun convert(inputImage: BufferedImage, formatType: String, output: OutputStream) {
        ImageIO.write(inputImage, formatType, output)
    }

    /**
     * 图像切割（指定切片的行数和列数）
     *
     * @param inputImage 源图像地址
     * @param destDir    切片目标文件夹
     * @param formatType 目标格式
     * @param rows       目标切片行数。默认2，必须是范围 [1, 20] 之内
     * @param cols       目标切片列数。默认2，必须是范围 [1, 20] 之内
     */
    fun cut(inputImage: BufferedImage, destDir: String, formatType: String, rows: Int, cols: Int) {
        var rows = rows
        var cols = cols
        if (rows <= 0 || rows > 20)
            rows = 2 // 切片行数
        if (cols <= 0 || cols > 20)
            cols = 2 // 切片列数
        // 读取源图像
        //BufferedImage bi = ImageIO.read(new File(srcImageFile));
        val w = inputImage.height // 源图宽度
        val h = inputImage.width // 源图高度
        if (w > 0 && h > 0) {
            var img: Image
            var cropFilter: ImageFilter
            val image = inputImage.getScaledInstance(w, h,
                    Image.SCALE_DEFAULT)
            var destWidth = w // 每张切片的宽度
            var destHeight = h // 每张切片的高度
            // 计算切片的宽度和高度
            if (w % cols == 0) {
                destWidth = w / cols
            } else {
                destWidth = Math.floor((w / cols).toDouble()).toInt() + 1
            }
            if (h % rows == 0) {
                destHeight = h / rows
            } else {
                destHeight = Math.floor((h / rows).toDouble()).toInt() + 1
            }
            // 循环建立切片
            // 改进的想法:是否可用多线程加快切割速度
            for (i in 0 until rows) {
                for (j in 0 until cols) {
                    // 四个参数分别为图像起点坐标和宽高
                    // 即: CropImageFilter(int x,int y,int width,int height)
                    cropFilter = CropImageFilter(j * destWidth, i * destHeight, destWidth, destHeight)
                    img = Toolkit.getDefaultToolkit().createImage(
                            FilteredImageSource(image.source, cropFilter))
                    val tag = BufferedImage(destWidth, destHeight, BufferedImage.TYPE_INT_ARGB)
                    val g = tag.graphics
                    g.drawImage(img, 0, 0, null) // 绘制缩小后的图
                    g.dispose()
                    // 输出为文件
                    ImageIO.write(tag, formatType, File(destDir + "_r" + i + "_c" + j + "." + formatType.toLowerCase()))
                }
            }
        }
    }

    /**
     * 给图片添加文字水印
     *
     * @param pressText     水印文字
     * @param input  源图像地址
     * @param output 目标图像地址
     * @param fontName      水印的字体名称
     * @param fontStyle     水印的字体样式
     * @param color         水印的字体颜色
     * @param fontSize      水印的字体大小
     * @param x             修正值
     * @param y             修正值
     * @param alpha         透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
     * @param formatType    目标格式
     */
    fun pressText(pressText: String, input: InputStream,
                  output: OutputStream, fontName: String, fontStyle: Int, color: Color,
                  fontSize: Int, x: Int, y: Int, alpha: Float, formatType: String) {
        val src = ImageIO.read(input)
        val width = src.getWidth(null)
        val height = src.getHeight(null)
        val image = BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
        val g = image.createGraphics()
        g.drawImage(src, 0, 0, width, height, null)
        g.color = color
        g.font = Font(fontName, fontStyle, fontSize)
        g.composite = AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
                alpha)
        // 在指定坐标绘制水印文字
        g.drawString(pressText, (width - getLength(pressText) * fontSize) / 2 + x, (height - fontSize) / 2 + y)
        g.dispose()
        ImageIO.write(image, formatType, output)// 输出到文件流
    }

    /**
     * 给图片添加图片水印
     *
     * @param pressImg      水印图片
     * @param input  源图像地址
     * @param output 目标图像地址
     * @param x             修正值。 默认在中间
     * @param y             修正值。 默认在中间
     * @param alpha         透明度：alpha 必须是范围 [0.0, 1.0] 之内（包含边界值）的一个浮点数字
     * @param formatType    目标格式
     */
    fun pressImage(pressImg: String, input: InputStream, output: OutputStream, x: Int, y: Int, alpha: Float, formatType: String) {
        val src = ImageIO.read(input)
        val wideth = src.getWidth(null)
        val height = src.getHeight(null)
        val image = BufferedImage(wideth, height,
                BufferedImage.TYPE_INT_ARGB)
        val g = image.createGraphics()
        g.drawImage(src, 0, 0, wideth, height, null)
        // 水印文件
        val src_biao = ImageIO.read(File(pressImg))
        val wideth_biao = src_biao.getWidth(null)
        val height_biao = src_biao.getHeight(null)
        g.composite = AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha)
        g.drawImage(src_biao, (wideth - wideth_biao) / 2,
                (height - height_biao) / 2, wideth_biao, height_biao, null)
        // 水印文件结束
        g.dispose()
        ImageIO.write(image, formatType, output)
    }

    /**
     * 计算text的长度（一个中文算两个字符）
     *
     * @param text
     * @return
     */
    fun getLength(text: String): Int {
        var length = 0
        for (i in 0 until text.length) {
            if (text[i].toString().toByteArray().size > 1) {
                length += 2
            } else {
                length += 1
            }
        }
        return length / 2
    }
}