package com.rollong.common.request

import com.rollong.common.sql.entity.AbstractEntity
import java.io.Serializable

interface CreateOrUpdateRequest<Entity : AbstractEntity<PK>, PK : Serializable> {
    val entityId: PK?
    fun applyCreate(): Entity
    fun applyUpdate(e: Entity)
}