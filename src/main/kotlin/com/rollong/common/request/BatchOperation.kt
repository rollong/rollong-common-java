package com.rollong.common.request

import com.rollong.common.sql.entity.AbstractEntity

class BatchOperation<Entity : AbstractEntity<*>>(
        private val entities: Set<Entity>,
        requests: Set<CreateOrUpdateRequest<Entity, *>>
) {
    private var whenUpdated = fun(_: Entity) {}

    private var whenCreated = fun(_: Entity) {}

    private var whenSkipped = fun(_: Entity) {}

    private var whenMissed = fun(_: CreateOrUpdateRequest<Entity, *>) {}

    private val requests = requests.toMutableSet()

    fun whenUpdated(f: (Entity) -> Unit): BatchOperation<Entity> {
        this.whenUpdated = f
        return this
    }

    fun whenCreated(f: (Entity) -> Unit): BatchOperation<Entity> {
        this.whenCreated = f
        return this
    }

    fun whenSkipped(f: (Entity) -> Unit): BatchOperation<Entity> {
        this.whenSkipped = f
        return this
    }

    fun whenMissed(f: (CreateOrUpdateRequest<Entity, *>) -> Unit): BatchOperation<Entity> {
        this.whenMissed = f
        return this
    }

    fun operate() {
        this.entities.forEach {
            val entity = it
            val request = this.requests.filter { null != it.entityId && it.entityId == entity.id }.firstOrNull()
            if (null != request) {
                request.applyUpdate(entity)
                this.whenUpdated(entity)
                this.requests.remove(request)
            } else {
                this.whenSkipped(entity)
            }
        }
        this.requests.filter { null == it.entityId }.forEach {
            val entity = it.applyCreate()
            this.whenCreated(entity)
            this.requests.remove(it)
        }
        this.requests.forEach(this.whenMissed)
    }
}

fun <Entity : AbstractEntity<*>> Set<Entity>.batchOperate(requests: Set<CreateOrUpdateRequest<Entity, *>>) = BatchOperation(entities = this, requests = requests)