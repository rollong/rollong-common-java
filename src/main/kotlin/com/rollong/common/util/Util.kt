package com.rollong.common.util

import com.github.pagehelper.Page
import com.github.pagehelper.PageHelper
import org.springframework.data.domain.Pageable
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.util.stream.Stream

inline fun <T : Any, R : Any> Page<T>.mapPage(crossinline transform: (T) -> R): Page<R> {
    val page = Page<R>(this.pageNum, this.pageSize)
    page.total = this.total
    page.startRow = this.startRow
    page.endRow = this.endRow
    page.pages = this.pages
    this.forEach {
        page.add(transform(it))
    }
    return page
}

fun InputStream.readAllToByteArray(): ByteArray {
    val result = ByteArrayOutputStream()
    val buffer = ByteArray(1024)
    while (true) {
        val length = this.read(buffer)
        if (-1 != length) {
            result.write(buffer, 0, length)
        } else {
            return result.toByteArray()
        }
    }
}

fun Iterable<String>.implode(glue: String = ","): String {
    val sb = StringBuffer()
    this.forEach {
        if (sb.isEmpty()) {
            sb.append(it)
        } else {
            sb.append("$glue$it")
        }
    }
    return sb.toString()
}

fun Array<String>.implode(glue: String = ","): String {
    val sb = StringBuffer()
    this.forEach {
        if (sb.isEmpty()) {
            sb.append(it)
        } else {
            sb.append("$glue$it")
        }
    }
    return sb.toString()
}

fun String?.like() = if (null == this) null else "%${this}%"

inline fun Int.repeats(something: (Int) -> Any) {
    for (i in 0.until(this)) {
        something(i)
    }
}

fun <E> Pageable.toPageHelper() = PageHelper.startPage<E>(this.pageNumber + 1, this.pageSize)

inline fun <E, R> Stream<E>.batch(size: Int, crossinline action: (index: Int, items: List<E>) -> Collection<R>): Stream<R> {
    val buffer = mutableListOf<E>()
    val results = mutableListOf<R>()
    var index = 0
    this.forEachOrdered {
        if (buffer.size < size) {
            buffer.add(it)
        } else {
            results.addAll(action.invoke(index++, buffer))
            buffer.clear()
        }
    }
    if (buffer.isNotEmpty()) {
        results.addAll(action.invoke(index, buffer))
    }
    return results.stream()
}