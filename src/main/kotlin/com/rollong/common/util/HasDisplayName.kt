package com.rollong.common.util

interface HasDisplayName {
    val displayName: String
}