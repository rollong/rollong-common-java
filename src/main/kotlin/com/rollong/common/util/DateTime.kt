package com.rollong.common.util

import org.joda.time.DateTime
import org.joda.time.Period
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

fun Long?.toTimestamp() = if (null == this) null else Timestamp(this)

fun String.toDatetime(
        pattern: String = "yyyy-MM-dd HH:mm:ss",
        zone: String = "GMT+8"
): DateTime {
    val df = SimpleDateFormat(pattern)
    df.timeZone = TimeZone.getTimeZone(zone)
    return DateTime(df.parse(this).time)
}

fun DateTime.startOfDay() = this.withTimeAtStartOfDay()

fun Date.format(
        pattern: String = "yyyy-MM-dd HH:mm:ss",
        zone: String = "GMT+8"
): String {
    val df = SimpleDateFormat(pattern)
    df.timeZone = TimeZone.getTimeZone(zone)
    return df.format(this)
}

fun DateTime.endOfDay() = this.millisOfDay().withMaximumValue()

fun DateTime.startOfMonth() = this.withDayOfMonth(1).startOfDay()

fun DateTime.endOfMonth() = this.withDayOfMonth(1).plusMonths(1).minusDays(1).endOfDay()

fun DateTime.startOfQuarter() = when (this.monthOfYear) {
    1, 2, 3 -> this.withDate(this.year, 1, 1).startOfMonth()
    4, 5, 6 -> this.withDate(this.year, 4, 1).startOfMonth()
    7, 8, 9 -> this.withDate(this.year, 7, 1).startOfMonth()
    else -> this.withDate(this.year, 10, 1).startOfMonth()
}

fun DateTime.endOfQuarter() = when (this.monthOfYear) {
    1, 2, 3 -> this.withDate(this.year, 3, 1).endOfMonth()
    4, 5, 6 -> this.withDate(this.year, 6, 1).endOfMonth()
    7, 8, 9 -> this.withDate(this.year, 9, 1).endOfMonth()
    else -> this.withDate(this.year, 12, 1).endOfMonth()
}

fun DateTime.startOfYear() = this.withDate(this.year, 1, 1).startOfDay()

fun DateTime.endOfYear() = this.withDate(this.year, 12, 31).endOfDay()

fun DateTime.convertToSqlTimestamp() = Timestamp(this.millis)

fun Date.convertToSqlTimestamp() = Timestamp(this.time)

fun Long?.toDateTime() = if (null != this) DateTime(this) else null

fun Date.toDateTime() = DateTime(this.time)

fun Date.epoch() = this.time / 1000

fun Int.millis() = Period.millis(this)

fun Int.seconds() = Period.seconds(this)

fun Int.minutes() = Period.minutes(this)

fun Int.hours() = Period.hours(this)

fun Int.days() = Period.days(this)

fun Int.months() = Period.months(this)

fun Int.years() = Period.years(this)

fun Period.ago() = DateTime().minus(this)

fun Period.later() = DateTime().plus(this)

fun Long.ago(timeUnit: TimeUnit) = DateTime(System.currentTimeMillis() - timeUnit.toMillis(this))

fun Long.later(timeUnit: TimeUnit) = DateTime(System.currentTimeMillis() + timeUnit.toMillis(this))

fun Date.compareToPeriod(from: Date?, to: Date?): Int {
    return when {
        this.time < from?.time ?: 0 -> -1
        this.time >= to?.time ?: Long.MAX_VALUE -> 1
        else -> 0
    }
}