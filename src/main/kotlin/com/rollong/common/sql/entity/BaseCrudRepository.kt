package com.rollong.common.sql.entity

import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean
import java.io.Serializable

@NoRepositoryBean
interface BaseCrudRepository<Entity : AbstractEntity<PK>, PK : Serializable> : CrudRepository<Entity, PK>