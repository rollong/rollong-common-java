package com.rollong.common.sql.usertype

import com.alibaba.fastjson.JSONObject

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/27 17:58
 * @project 诺朗java-common库
 * @filename JSONObjectUserType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql.usertype
 * @description  返回JSONObject
 */
class JSONObjectUserType : JSONUserType() {
    override fun returnedClass() = JSONObject::class.java
}