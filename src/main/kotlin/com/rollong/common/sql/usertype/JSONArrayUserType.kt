package com.rollong.common.sql.usertype

import com.alibaba.fastjson.JSONArray

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/08/27 17:59
 * @project 诺朗java-common库
 * @filename JSONArrayUserType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.sql.usertype
 * @description  返回JSONArray
 */
class JSONArrayUserType : JSONUserType() {
    override fun returnedClass() = JSONArray::class.java
}