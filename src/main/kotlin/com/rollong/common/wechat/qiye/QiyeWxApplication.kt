package com.rollong.common.wechat.qiye

import me.chanjar.weixin.cp.api.WxCpService
import me.chanjar.weixin.cp.config.WxCpConfigStorage

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2020/01/23 11:47
 * @project common
 * @filename QiyeWxApplication.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.wechat.qiye
 * @description
 */
interface QiyeWxApplication {
    val wxCpService: WxCpService
    val configStorage: WxCpConfigStorage
}