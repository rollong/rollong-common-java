package com.rollong.common.wechat.qiye

import me.chanjar.weixin.cp.api.WxCpService
import me.chanjar.weixin.cp.api.impl.WxCpServiceImpl
import me.chanjar.weixin.cp.config.WxCpConfigStorage
import me.chanjar.weixin.cp.config.impl.WxCpDefaultConfigImpl

class QiyeWxApplicationImpl(
        config: QiyeWxConfig
) : QiyeWxApplication {
    override val wxCpService: WxCpService
    override val configStorage: WxCpConfigStorage

    init {
        this.configStorage = WxCpDefaultConfigImpl().also {
            it.corpId = config.corpId
            it.corpSecret = config.corpSecret
            it.agentId = config.agentId
            it.token = config.token
            it.aesKey = config.aesKey
        }
        this.wxCpService = WxCpServiceImpl().also {
            it.wxCpConfigStorage = this.configStorage
        }
    }
}