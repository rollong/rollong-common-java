package com.rollong.common.wechat.qiye

class QiyeWxConfig(
        var corpId: String = "",
        var corpSecret: String = "",
        var agentId: Int = 0,
        var token: String = "",
        var aesKey: String = ""
)