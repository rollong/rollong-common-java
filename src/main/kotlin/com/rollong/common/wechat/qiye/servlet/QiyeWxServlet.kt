package com.rollong.common.wechat.qiye.servlet

import com.rollong.common.wechat.qiye.QiyeWxApplication
import me.chanjar.weixin.cp.bean.WxCpXmlMessage
import me.chanjar.weixin.cp.message.WxCpMessageRouter
import me.chanjar.weixin.cp.util.crypto.WxCpCryptUtil
import org.slf4j.LoggerFactory
import java.io.IOException
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * 服务端事件消息接收
 *
 * @link https://github.com/Wechat-Group/WxJava/wiki/CP_Quick-Start
 */
class QiyeWxServlet(
        private val app: QiyeWxApplication,
        route: (WxCpMessageRouter) -> Unit
) {
    private val logger = LoggerFactory.getLogger(QiyeWxServlet::class.java)
    private val router: WxCpMessageRouter = WxCpMessageRouter(this.app.wxCpService).let { route.invoke(it);it }

    @Throws(ServletException::class, IOException::class)
    fun serve(request: HttpServletRequest, response: HttpServletResponse) {
        response.contentType = "text/html;charset=utf-8"
        response.status = HttpServletResponse.SC_OK
        val msgSignature = request.getParameter("msg_signature") ?: ""
        val nonce = request.getParameter("nonce") ?: ""
        val timestamp = request.getParameter("timestamp") ?: ""
        val echostr = request.getParameter("echostr") ?: ""
        if (echostr.isNotBlank()) {
            if (!this.app.wxCpService.checkSignature(msgSignature, timestamp, nonce, echostr)) {
                //消息签名不正确，说明不是公众平台发过来的消息
                response.writer.println("非法请求")
                this.logger.error("非法请求,消息签名不正确，说明不是公众平台发过来的消息")
            } else {
                val cryptUtil = WxCpCryptUtil(this.app.configStorage)
                val plainText = cryptUtil.decrypt(echostr)
                //说明是一个仅仅用来验证的请求，回显echostr
                response.writer.println(plainText)
                this.logger.debug("说明是一个仅仅用来验证的请求,echostr=${echostr}/planText=${plainText}")
            }
        } else {
            val inMessage = WxCpXmlMessage.fromEncryptedXml(request.inputStream, this.app.configStorage, timestamp, nonce, msgSignature)
            val outMessage = this.router.route(inMessage)
            if (null != outMessage) {
                response.writer.write(outMessage.toEncryptedXml(this.app.configStorage))
                this.logger.debug("存在返回")
            } else {
                this.logger.debug("route没有截获到处理方法")
            }
        }
    }
}