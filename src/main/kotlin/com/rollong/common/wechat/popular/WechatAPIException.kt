package com.rollong.common.wechat.popular

import com.rollong.common.exception.APIRuntimeException
import weixin.popular.bean.BaseResult

class WechatAPIException(
        val resp: BaseResult,
        message: String? = null
) : APIRuntimeException(message = message ?: "微信接口返回错误 ${resp.errcode}:${resp.errmsg}")