package com.rollong.common.wechat.popular.servlet

import weixin.popular.bean.message.EventMessage
import weixin.popular.bean.xmlmessage.XMLMessage
import weixin.popular.bean.xmlmessage.XMLTextMessage

class DumbHandler : WechatMessageHandler {

    override fun handle(message: EventMessage): XMLMessage? {
        return XMLTextMessage(
                message.fromUserName,
                message.toUserName,
                "你好")
    }
}