package com.rollong.common.wechat.popular.servlet

import weixin.popular.bean.message.EventMessage
import weixin.popular.bean.xmlmessage.XMLMessage

interface WechatMessageHandler {
    fun handle(message: EventMessage): XMLMessage?
}