package com.rollong.common.wechat.popular

import com.rollong.common.util.batch
import weixin.popular.api.MessageAPI
import weixin.popular.api.QrcodeAPI
import weixin.popular.api.SnsAPI
import weixin.popular.api.UserAPI
import weixin.popular.bean.message.templatemessage.TemplateMessage
import weixin.popular.bean.message.templatemessage.WxopenTemplateMessage
import weixin.popular.bean.sns.Jscode2sessionResult
import weixin.popular.bean.user.User
import kotlin.streams.toList

object WechatAPIUtil {

    @Throws(WechatAPIException::class)
    fun openIdByCode(
            code: String,
            wechatConfig: WechatConfig
    ): String {
        val resp = SnsAPI.jscode2session(
                wechatConfig.appId,
                wechatConfig.appSecret,
                code
        )
        if (resp.isSuccess) {
            return resp.openid
        } else {
            throw WechatAPIException(resp = resp)
        }
    }

    @Throws(WechatAPIException::class)
    fun jscode2session(
            code: String,
            wechatConfig: WechatConfig
    ): Jscode2sessionResult {
        val resp = SnsAPI.jscode2session(
                wechatConfig.appId,
                wechatConfig.appSecret,
                code
        )
        if (resp.isSuccess) {
            return resp
        } else {
            throw WechatAPIException(resp = resp)
        }
    }

    @Throws(WechatAPIException::class)
    fun userinfo(
            openId: String,
            wechatConfig: WechatConfig
    ): User {
        val accessToken = WechatTokenHelper.getToken(appId = wechatConfig.appId)
        val wechatUser = UserAPI.userInfo(accessToken, openId)
        if (wechatUser.isSuccess) {
            if (null == wechatUser.openid) {
                throw WechatAPIException(message = "没有获取到用户openid信息", resp = wechatUser)
            }
            return wechatUser
        } else {
            throw WechatAPIException(resp = wechatUser)
        }
    }

    @Throws(WechatAPIException::class)
    fun batchUserinfo(
            openIds: List<String>,
            wechatConfig: WechatConfig
    ): List<User> = openIds.stream().batch(100) { _, items ->
        val resp = UserAPI.userInfoBatchget(
                WechatTokenHelper.getToken(appId = wechatConfig.appId),
                "zh_CN",
                items,
                0
        )
        if (!resp.isSuccess) {
            throw WechatAPIException(resp = resp)
        }
        resp.user_info_list
    }.toList()

    class WxQrCode(
            var url: String? = null,
            var ticket: String? = null,
            var expireSeconds: Int? = null
    )

    //https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1443433542
    @Throws(WechatAPIException::class)
    fun temporaryQrCode(
            expireSeconds: Int = 300,
            sceneStr: String,
            wechatConfig: WechatConfig
    ): WxQrCode {
        val resp = QrcodeAPI.qrcodeCreateTemp(
                WechatTokenHelper.getToken(appId = wechatConfig.appId),
                expireSeconds,
                sceneStr
        )
        return if (resp.isSuccess) {
            WxQrCode(
                    url = resp.url,
                    ticket = resp.ticket,
                    expireSeconds = resp.expire_seconds
            )
        } else {
            throw WechatAPIException(resp = resp)
        }
    }

    @Throws(WechatAPIException::class)
    fun sendTemplateMessage(
            wechatConfig: WechatConfig,
            message: TemplateMessage
    ) {
        val resp = MessageAPI.messageTemplateSend(
                WechatTokenHelper.getToken(appId = wechatConfig.appId),
                message
        )
        if (!resp.isSuccess) {
            throw WechatAPIException(resp = resp)
        }
    }

    @Throws(WechatAPIException::class)
    fun sendMiniProgramTemplateMessage(
            wechatConfig: WechatConfig,
            message: WxopenTemplateMessage
    ) {
        val resp = MessageAPI.messageWxopenTemplateSend(
                WechatTokenHelper.getToken(appId = wechatConfig.appId),
                message
        )
        if (!resp.isSuccess) {
            throw WechatAPIException(resp = resp)
        }
    }

    @Throws(WechatAPIException::class)
    fun allOpenIdGet(
            wechatConfig: WechatConfig
    ): List<String> {
        var nextOpenId: String? = null
        val openIds = mutableListOf<String>()
        while (true) {
            val resp = UserAPI.userGet(
                    WechatTokenHelper.getToken(appId = wechatConfig.appId),
                    nextOpenId
            )
            nextOpenId = resp.next_openid
            if (!resp.isSuccess) {
                throw WechatAPIException(resp = resp)
            }
            if (resp.count > 0) {
                openIds.addAll(resp.data.openid)
            } else {
                break
            }
        }
        return openIds
    }

    @Throws(WechatAPIException::class)
    fun allUserGet(
            wechatConfig: WechatConfig
    ) = batchUserinfo(
            openIds = allOpenIdGet(wechatConfig = wechatConfig),
            wechatConfig = wechatConfig
    )
}