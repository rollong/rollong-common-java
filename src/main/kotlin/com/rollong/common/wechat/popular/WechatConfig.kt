package com.rollong.common.wechat.popular

class WechatConfig(
        var appId: String = "",
        var appSecret: String = "",
        var isMiniProgram: Boolean = false,
        var token: String? = null,
        var aesKey: String? = null,
        var appName: String? = null,
        var note: String? = null
)