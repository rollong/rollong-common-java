package com.rollong.common.wechat.popular.servlet

import com.qq.weixin.mp.aes.AesException
import com.qq.weixin.mp.aes.WXBizMsgCrypt
import com.rollong.common.exception.APIRuntimeException
import com.rollong.common.wechat.popular.WechatConfig
import weixin.popular.bean.message.EventMessage
import weixin.popular.support.expirekey.DefaultExpireKey
import weixin.popular.util.SignatureUtil
import weixin.popular.util.StreamUtils
import weixin.popular.util.XMLConverUtil
import java.io.IOException
import java.io.OutputStream
import java.net.URLDecoder
import java.nio.charset.Charset
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/**
 * 服务端事件消息接收
 *
 * @author Yi
 */
class WechatServlet(
        private val config: WechatConfig,
        private val handlers: List<WechatMessageHandler> = listOf(DumbHandler())
) {

    private val appId: String
        get() = this.config.appId
    private val token: String
        get() = this.config.token ?: throw APIRuntimeException("没有配置token")
    private val encodingAesKey: String
        get() = this.config.aesKey ?: throw APIRuntimeException("没有配置aesKey")

    //重复通知过滤
    private val expireKey = DefaultExpireKey()

    @Throws(IOException::class, AesException::class)
    fun doPost(request: HttpServletRequest, response: HttpServletResponse) {
        val inputStream = request.inputStream
        val outputStream = response.outputStream
        val signature = request.getParameter("signature")
        val timestamp = request.getParameter("timestamp")
        val nonce = request.getParameter("nonce")
        var echostr = request.getParameter("echostr")
        //加密模式
        val msgSignature = request.getParameter("msg_signature")
        //加密方式
        val isAes = "aes" == request.getParameter("encrypt_type")
        val wxBizMsgCrypt = if (isAes) {
            WXBizMsgCrypt(this.token, this.encodingAesKey, this.appId)
        } else {
            null
        }
        if (null != echostr) {
            //首次请求申请验证,返回echostr
            if (isAes) {
                echostr = URLDecoder.decode(echostr, "utf-8")
                val echoStrDecrypt = wxBizMsgCrypt!!.verifyUrl(msgSignature, timestamp, nonce, echostr)
                this.outputStreamWrite(outputStream, echoStrDecrypt)
            } else {
                this.outputStreamWrite(outputStream, echostr)
            }
        } else {
            val eventMessage: EventMessage =
                    if (isAes) {
                        //获取XML数据（含加密参数）
                        val postData = StreamUtils.copyToString(inputStream, Charset.forName("utf-8"))
                        //解密XML 数据
                        val xmlData = wxBizMsgCrypt!!.decryptMsg(msgSignature, timestamp, nonce, postData)
                        //XML 转换为bean 对象
                        XMLConverUtil.convertToObject(EventMessage::class.java, xmlData)
                    } else {
                        //验证请求签名
                        if (null == timestamp || null == nonce) {
                            throw APIRuntimeException("微信servlet签名认证不正确(POST,non-aes)timestamp/nonce为空")
                        }
                        if (signature != SignatureUtil.generateEventMessageSignature(token, timestamp, nonce)) {
                            throw APIRuntimeException("微信servlet签名认证不正确(POST,non-aes)")
                        }
                        //XML 转换为bean 对象
                        XMLConverUtil.convertToObject(EventMessage::class.java, inputStream)
                    }

            val key = "${eventMessage.fromUserName}__${eventMessage.toUserName}__${eventMessage.msgId}__${eventMessage.createTime}"
            if (this.expireKey.exists(key)) {
                //重复通知不作处理
                return
            } else {
                this.expireKey.add(key)
            }
            for (handler in this.handlers) {
                val resp = handler.handle(eventMessage)
                if (null != resp) {
                    resp.outputStreamWrite(outputStream, wxBizMsgCrypt)
                    break
                }
            }
        }
    }

    @Throws(IOException::class)
    fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
        val outputStream = response.outputStream
        val signature = request.getParameter("signature")
        val timestamp = request.getParameter("timestamp")
        val nonce = request.getParameter("nonce")
        val echostr = request.getParameter("echostr")
        if (echostr != null && null == signature) {
            //首次请求申请验证,返回echostr
            outputStreamWrite(outputStream, echostr)
        } else if (null == timestamp || null == nonce) {
            throw APIRuntimeException("微信servlet签名认证不正确(GET,non-aes)timestamp/nonce为空")
        } else if (signature != SignatureUtil.generateEventMessageSignature(this.token, timestamp, nonce)) {
            //验证请求签名
            throw APIRuntimeException("微信servlet签名认证不正确(GET,non-aes)")
        } else {
            this.outputStreamWrite(outputStream, echostr)
        }
    }

    /**
     * 数据流输出
     * @param outputStream
     * @param text
     * @return
     */
    private fun outputStreamWrite(outputStream: OutputStream, text: String) {
        outputStream.write(text.toByteArray(charset("utf-8")))
    }
}