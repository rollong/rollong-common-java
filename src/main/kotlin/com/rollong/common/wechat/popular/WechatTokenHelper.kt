package com.rollong.common.wechat.popular

import com.rollong.common.exception.BadRequestException
import java.util.concurrent.ConcurrentHashMap

object WechatTokenHelper {

    private val clients = ConcurrentHashMap<String, WechatTokenClient>()

    fun addWxApp(config: WechatConfig) {
        this.clients[config.appId] = WechatTokenClient(config = config)
    }

    fun removeWxApp(appId: String) {
        this.clients.remove(appId)
    }

    fun getToken(appId: String) = this.getToken(appId = appId, forceReload = false)

    fun getToken(appId: String, forceReload: Boolean = false): String {
        val client = this.clients[appId]
        if (null != client) {
            return client.getToken(forceReload)
        }
        throw BadRequestException("没有设置appId:$appId")
    }

    fun getTicket(appId: String, type: String = "jsapi", forceReload: Boolean = false): String {
        val client = this.clients[appId]
        if (null != client) {
            return client.getTicket(type = type, forceReload = forceReload)
        }
        throw BadRequestException("没有设置appId:$appId")
    }
}