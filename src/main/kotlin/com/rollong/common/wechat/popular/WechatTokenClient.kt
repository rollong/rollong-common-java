package com.rollong.common.wechat.popular

import com.alibaba.fastjson.JSON
import com.rollong.common.exception.APIRuntimeException
import com.rollong.common.util.readAllToByteArray
import org.apache.http.client.methods.RequestBuilder
import org.apache.http.entity.ContentType
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import java.util.concurrent.ConcurrentHashMap

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2019/10/15 18:36
 * @project 诺朗java-common库
 * @filename WechatTokenClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.common.wechat.popular
 * @description  微信获取token服务
 */
class WechatTokenClient(
        private val config: WechatConfig
) {

    private val host = "https://api.wx.rollong.com/api/v1/"

    private val tokens = ConcurrentHashMap<String, Token>()

    data class Token(
            var token: String = "",
            var expiresAt: Long = 0L
    ) {
        fun isValid() = System.currentTimeMillis() - 60000L <= this.expiresAt
    }

    data class Response(
            var code: Int? = null,
            var items: Token? = null,
            var message: String? = null
    )

    fun clearCache() {
        this.tokens.clear()
    }

    fun getToken(forceReload: Boolean = false): String {
        if (forceReload) {
            this.tokens.remove("access_token")
        } else {
            this.tokens["access_token"]?.let {
                if (it.isValid()) {
                    return it.token
                }
            }
        }
        val client = HttpClients.createDefault()
        val reqBody = mapOf(
                "appId" to this.config.appId,
                "appSecret" to this.config.appSecret,
                "type" to if (this.config.isMiniProgram) "MiniProgram" else "Popular",
                "note" to this.config.note,
                "appName" to this.config.appName,
                "forceReload" to forceReload
        )
        val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
        val request = RequestBuilder
                .post()
                .setUri("${this.host}wx/getAccessToken")
                .addHeader("Content-Type", "application/json")
                .setEntity(entity)
                .build()
        val response = client.execute(request)
        val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
        val resp = JSON.parseObject(respBody, Response::class.java)
        if (0 == resp.code) {
            resp.items?.let {
                this.tokens["access_token"] = it
                return it.token
            }
            throw APIRuntimeException("获取token报错${resp.message}")
        } else {
            throw APIRuntimeException("获取token报错${resp.message}")
        }
    }

    fun getTicket(type: String = "jsapi", forceReload: Boolean = false): String {
        val key = "ticket_${type}"
        if (forceReload) {
            this.tokens.remove(key)
        } else {
            this.tokens[key]?.let {
                if (it.isValid()) {
                    return it.token
                }
            }
        }
        val client = HttpClients.createDefault()
        val reqBody = mapOf(
                "appId" to this.config.appId,
                "appSecret" to this.config.appSecret,
                "type" to type,
                "forceReload" to forceReload
        )
        val entity = StringEntity(JSON.toJSONString(reqBody), ContentType.APPLICATION_JSON)
        val request = RequestBuilder
                .post()
                .setUri("${this.host}wx/getTicket")
                .addHeader("Content-Type", "application/json")
                .setEntity(entity)
                .build()
        val response = client.execute(request)
        val respBody = response.entity.content.readAllToByteArray().toString(Charsets.UTF_8)
        val resp = JSON.parseObject(respBody, Response::class.java)
        if (0 == resp.code) {
            resp.items?.let {
                this.tokens[key] = it
                return it.token
            }
            throw APIRuntimeException("获取ticket报错${resp.message}")
        } else {
            throw APIRuntimeException("获取ticket报错${resp.message}")
        }
    }

}