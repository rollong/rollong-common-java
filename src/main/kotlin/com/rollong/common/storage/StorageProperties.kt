package com.rollong.common.storage

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@ConfigurationProperties(prefix = "storage")
@Component
class StorageProperties(
        var disks: MutableMap<String, Map<String, String>> = mutableMapOf()
) {
    fun getStorageDisk(name: String): StorageDisk {
        this.disks[name]?.let {
            return when (it["driver"]) {
                "local" -> LocalDisk(
                        config = LocalDiskConfig(
                                publicURL = it["publicURL"] ?: throw RuntimeException("未指定publicURL"),
                                localPath = it["localPath"] ?: throw RuntimeException("未指定localPath")
                        )
                )
                "oss" -> OssBucket(
                        config = OssBucketConfig(
                                endpoint = it["endpoint"] ?: throw RuntimeException("未指定endpoint"),
                                accessKey = it["accessKey"] ?: throw RuntimeException("未指定accessKey"),
                                accessSecret = it["accessSecret"] ?: throw RuntimeException("未指定accessSecret"),
                                bucketName = it["bucketName"] ?: throw RuntimeException("未指定bucketName"),
                                publicAcl = (it["publicAcl"]?.toBoolean()) ?: false,
                                publicURL = it["publicURL"] ?: throw RuntimeException("未指定publicURL"),
                                prefix = it["prefix"] ?: ""
                        )
                )
                else -> throw RuntimeException("没有找到storageDisk驱动:${it["driver"]}")
            }
        }
        throw RuntimeException("没有找到storageDisk配置:$name")
    }
}