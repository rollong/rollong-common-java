package com.rollong.common.storage

data class LocalDiskConfig(
        var publicURL: String = "",
        var localPath: String = ""
)