package com.rollong.common.storage

data class OssBucketConfig(
        var endpoint: String = "",
        var accessKey: String = "",
        var accessSecret: String = "",
        var bucketName: String = "",
        var publicAcl: Boolean = false,
        var publicURL: String = "",
        var prefix: String = ""
)