package com.rollong.common.storage

import com.rollong.common.exception.NotFoundException
import java.io.InputStream
import java.util.concurrent.TimeUnit

interface StorageDisk {

    fun write(path: String, stream: InputStream)

    @Throws(NotFoundException::class)
    fun read(path: String): InputStream

    fun exists(filename: String): Boolean

    fun delete(path: String)

    fun publicURL(path: String, style: String? = null, timeout: Long = 1, timeUnit: TimeUnit = TimeUnit.DAYS): String

    fun list(path: String, limit: Int? = null): List<FileData>

    data class FileData(
            var name: String? = null,
            var createdAt: Long? = null
    )
}