package com.rollong.common.task

class ThreadPoolTaskSchedulerConfig(
        var poolSize: Int = 1,
        var threadNamePrefix: String? = null,
        /**
         * @see org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler.setWaitForTasksToCompleteOnShutdown
         */
        var waitForTasksToCompleteOnShutdown: Boolean? = null,
        /**
         * @see org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler.setAwaitTerminationSeconds
         */
        var awaitTerminationSeconds: Int? = null
)