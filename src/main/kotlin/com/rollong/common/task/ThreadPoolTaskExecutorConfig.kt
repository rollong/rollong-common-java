package com.rollong.common.task

class ThreadPoolTaskExecutorConfig(
        var corePoolSize: Int = 1,
        var maxPoolSize: Int = Int.MAX_VALUE,
        var queueCapacity: Int = Int.MAX_VALUE,
        var keepAliveSeconds: Int = 60,
        var threadNamePrefix: String? = null
)