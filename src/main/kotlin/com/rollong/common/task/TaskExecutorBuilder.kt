package com.rollong.common.task

import com.rollong.common.exception.ExceptionReporter
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import java.util.concurrent.Executor
import java.util.concurrent.ThreadPoolExecutor

/**
 * @link: https://blog.csdn.net/xingbaozhen1210/article/details/78717692
 */
object TaskExecutorBuilder {

    fun getAsyncConfigurer(
            config: ThreadPoolTaskExecutorConfig,
            reporter: ExceptionReporter
    ) = object : AsyncConfigurer {
        override fun getAsyncExecutor(): Executor = TaskExecutorBuilder.taskExecutor(config = config)
        override fun getAsyncUncaughtExceptionHandler() = AsyncUncaughtExceptionHandler { ex, method, params -> reporter.report(ex, method, params) }
    }

    fun getSchedulingConfigurer(
            taskScheduler: ThreadPoolTaskScheduler
    ) = SchedulingConfigurer { taskRegistrar -> taskRegistrar.setTaskScheduler(taskScheduler) }

    fun taskExecutor(
            config: ThreadPoolTaskExecutorConfig
    ): ThreadPoolTaskExecutor {
        val executor = ThreadPoolTaskExecutor()
        executor.corePoolSize = config.corePoolSize
        executor.maxPoolSize = config.maxPoolSize
        executor.setQueueCapacity(config.queueCapacity)
        executor.keepAliveSeconds = config.keepAliveSeconds
        executor.threadNamePrefix = config.threadNamePrefix ?: "taskExecutor-"

        // rejection-policy：当pool已经达到max size的时候，如何处理新任务
        // CALLER_RUNS：不在新线程中执行任务，而是由调用者所在的线程来执行
        executor.setRejectedExecutionHandler(ThreadPoolExecutor.CallerRunsPolicy())
        executor.initialize()
        return executor
    }

    /**
     * 并行任务使用策略：多线程处理
     *
     * @return ThreadPoolTaskScheduler 线程池
     */
    fun taskScheduler(
            config: ThreadPoolTaskSchedulerConfig
    ): ThreadPoolTaskScheduler {
        val scheduler = ThreadPoolTaskScheduler()
        scheduler.poolSize = config.poolSize
        scheduler.threadNamePrefix = config.threadNamePrefix ?: "taskScheduler-"
        config.awaitTerminationSeconds?.let {
            scheduler.setAwaitTerminationSeconds(it)
        }
        config.waitForTasksToCompleteOnShutdown?.let {
            scheduler.setWaitForTasksToCompleteOnShutdown(it)
        }
        return scheduler
    }
}