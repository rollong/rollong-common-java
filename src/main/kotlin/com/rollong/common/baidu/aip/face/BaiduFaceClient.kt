package com.rollong.common.baidu.aip.face

import com.baidu.aip.face.AipFace
import com.baidu.aip.face.FaceVerifyRequest
import com.baidu.aip.face.MatchRequest
import com.rollong.common.baidu.aip.BaiduAIPException
import com.rollong.common.exception.APIRuntimeException
import com.rollong.common.util.base64encode
import com.rollong.common.util.readAllToByteArray
import org.json.JSONObject
import java.io.InputStream

class BaiduFaceClient(
        private val appId: String,
        private val apiKey: String,
        private val secretKey: String
) {

    private val client = AipFace(this.appId, this.apiKey, this.secretKey)

    enum class QualityControl {
        NONE, LOW, NORMAL, HIGH
    }

    enum class LivenessControl {
        NONE, LOW, NORMAL, HIGH
    }

    private fun checkResp(resp: JSONObject): JSONObject {
        val errorCode = if (resp.has("error_code")) {
            resp.get("error_code").toString()
        } else {
            null
        }
        if (null != errorCode && "0" != errorCode) {
            val errorMsg = if (resp.has("error_msg")) {
                resp.get("error_msg").toString()
            } else {
                ""
            }
            throw BaiduAIPException(baiduErrCode = errorCode, baiduErrMsg = errorMsg)
        }
        return resp.getJSONObject("result")
    }

    fun addFace(
            image: InputStream,
            userId: String,
            groupId: String,
            userInfo: String? = null,
            qualityControl: QualityControl = QualityControl.NONE,
            livenessControl: LivenessControl = LivenessControl.NONE
    ) {
        val options = hashMapOf(
                "quality_control" to qualityControl.name,
                "liveness_control" to livenessControl.name
        )
        if (null != userInfo) {
            options["user_info"] = userInfo
        }
        val resp = this.client.addUser(
                image.readAllToByteArray().base64encode(),
                "BASE64",
                groupId,
                userId,
                options
        )
        this.checkResp(resp)
    }

    enum class ActionType {
        UPDATE, REPLACE
    }

    fun updateFace(
            image: InputStream,
            userId: String,
            groupId: String,
            userInfo: String? = null,
            qualityControl: QualityControl = QualityControl.NONE,
            livenessControl: LivenessControl = LivenessControl.NONE,
            actionType: ActionType = ActionType.UPDATE
    ) {
        val options = hashMapOf(
                "quality_control" to qualityControl.name,
                "liveness_control" to livenessControl.name,
                "action_type" to actionType.name
        )
        if (null != userInfo) {
            options["user_info"] = userInfo
        }
        val resp = this.client.updateUser(
                image.readAllToByteArray().base64encode(),
                "BASE64",
                groupId,
                userId,
                options
        )
        this.checkResp(resp)
    }

    fun deleteUser(
            userId: String,
            groupId: String
    ) {
        val resp = this.client.deleteUser(userId, groupId, null)
        this.checkResp(resp)
    }

    class Face(
            var image: String = "",
            var faceType: FaceType = FaceType.LIVE,
            var qualityControl: QualityControl = QualityControl.NONE,
            var livenessControl: LivenessControl = LivenessControl.NONE
    ) {
        fun toMatchRequest() = MatchRequest(
                image,
                "BASE64",
                this.faceType.name,
                this.qualityControl.name,
                this.livenessControl.name
        )
    }

    enum class FaceType {
        LIVE, IDCARD, WATERMARK, CERT
    }

    fun match(
            face1: Face,
            face2: Face
    ): Double {
        val resp = this.client.match(arrayListOf(face1.toMatchRequest(), face2.toMatchRequest()))
        return this.checkResp(resp).getDouble("score") / 100.0
    }

    fun liveness(
            image: InputStream
    ): Double {
        val req = FaceVerifyRequest(image.readAllToByteArray().base64encode(), "BASE64", "age,beauty,expression,faceshape,gender,glasses,landmark,race,quality,facetype")
        val resp = this.client.faceverify(arrayListOf(req))
        return this.checkResp(resp).getDouble("face_liveness")
    }

    fun check(
            userId: String,
            groupId: String,
            image: InputStream,
            qualityControl: QualityControl = QualityControl.NONE,
            livenessControl: LivenessControl = LivenessControl.NONE
    ): Double {
        val faceListResp = this.client.faceGetlist(userId, groupId, null)
        val face = (this.checkResp(faceListResp).getJSONArray("face_list").firstOrNull()
                ?: throw APIRuntimeException("人脸库中没有找到人脸信息")) as JSONObject
        val token = face.getString("face_token")
        val req0 = MatchRequest(token, "FACE_TOKEN")
        val req1 = MatchRequest(image.readAllToByteArray().base64encode(), "BASE64", FaceType.LIVE.name, qualityControl.name, livenessControl.name)
        val matchResp = this.client.match(arrayListOf(req0, req1))
        return this.checkResp(matchResp).getDouble("score") / 100.0
    }
}