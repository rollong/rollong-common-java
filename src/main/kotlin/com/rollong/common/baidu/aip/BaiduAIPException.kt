package com.rollong.common.baidu.aip

import com.rollong.common.exception.APIRuntimeException

class BaiduAIPException(
        val baiduErrCode: String,
        val baiduErrMsg: String
) : APIRuntimeException(
        message = "$baiduErrCode:[$baiduErrMsg]"
)